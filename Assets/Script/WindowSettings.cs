﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class WindowSettings : Singleton<WindowSettings>
{
    [SerializeField] private Slider _volumeBackgroundMusic;
    [SerializeField] private Slider _volumeSFX;
    [SerializeField] private Toggle _controlOnMouse;
    [SerializeField] private Toggle _controlOnTouch;
    [SerializeField] private Button _btnEscape;
    [SerializeField] private RectTransform _rectTransform;

    public bool isActived;
    
    private Canvas _canvasWindow = default;

    private void Awake() 
    {
        _rectTransform = GetComponent<RectTransform>();
        _canvasWindow = gameObject.GetComponent<Canvas>();
        _canvasWindow.enabled = false;
        isActived = _canvasWindow.enabled;
    }

    void Start()
    {  
        if (SceneManager.GetActiveScene().buildIndex == 2)
            _btnEscape.gameObject.SetActive(true);
        else
            _btnEscape.gameObject.SetActive(false);
    }


    private void Update() 
    {/*
        if(_controlOnMouse.isOn == false && _controlOnTouch.isOn == false)
        {
             if(CurrentSettings.instance.Platform.Equals("DesktopPlatform"))
                _controlOnMouse.isOn = true;
             else if(CurrentSettings.instance.Platform.Equals("MobilePlatform"))
                _controlOnTouch.isOn = true;
        }*/
    }

    public void ClosingWindowSettings()
    {
        var platform = "";

        if(_controlOnMouse.isOn == true)
        {
            platform ="DesktopPlatform";
            CurrentSettings.instance.Control = CurrentSettings.Controls.Mouse;
        }
        
        if(_controlOnTouch.isOn == true)
        {
            platform ="MobilePlatform";
            CurrentSettings.instance.Control = CurrentSettings.Controls.Swipe;
        }

        BootLoader.instance.settingsLoader.WriteSettings(platform, _volumeBackgroundMusic.value, _volumeSFX.value);
        _canvasWindow.enabled = false;

        InputManager.Focus = true;
    }

    public void OpeningWindowSettings()
    {

        if (CurrentSettings.instance.Platform.Equals("DesktopPlatform"))
            _controlOnMouse.isOn = true;
        else if (CurrentSettings.instance.Platform.Equals("MobilePlatform"))
            _controlOnTouch.isOn = true;
        

        if (CurrentSettings.instance != false)
        {
            if (CurrentSettings.instance.Control == CurrentSettings.Controls.Mouse)
                _controlOnMouse.isOn = true;
            else if (CurrentSettings.instance.Control == CurrentSettings.Controls.Swipe)
                _controlOnTouch.isOn = true;
            SoundSystem.instance.GetBackgroundAudioSource().volume = CurrentSettings.instance.VolumeBackgroundMuic;
            SoundSystem.instance.GetSFXAudioSource().volume = CurrentSettings.instance.VolumeSFX;
            _volumeBackgroundMusic.value = CurrentSettings.instance.VolumeBackgroundMuic;
            _volumeSFX.value = CurrentSettings.instance.VolumeSFX;
        }

        _canvasWindow.enabled = true;

    }

    public void SetButtonForCall(Button btn, Transform parent) 
    {
        _rectTransform.SetParent(parent);
        btn.onClick.AddListener(OnClick);

    }

    public void OnClick()
    {
        this.gameObject.transform.localScale = new Vector3(1, 1, 1);
        _canvasWindow.enabled = true;

        InputManager.Focus = false;

        if (SceneManager.GetActiveScene().buildIndex == 2)
            _btnEscape.gameObject.SetActive(true);
        else
            _btnEscape.gameObject.SetActive(false);
    }

    public void ClickEscape()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1, LoadSceneMode.Single);
    }
}
