﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchAnimationExplosion : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var position = animator.GetFloat("Position");
     //   var position = animator.GetFloat("Position");
     //   Debug.Log($"Before switch animation. StateMachine:{position}");

        if (position == 0.4f)
            animator.Play("ExplosionLine05");

        if (position == 0.3f || position == 0.6f)
            animator.Play("ExplosionLine03");

        if (position == 0.1f || position == 0.7f)
            animator.Play("ExplosionLine02");

        if (position == 0 || position == 0.9f)
            animator.Play("ExplosionLine01");

      //  Debug.Log($"After switch animation. StateMachine:{position}");
        animator.SetFloat("Position", 0.0f);
     //   Debug.Log($"Afterl null. StateMachine:{position}");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
