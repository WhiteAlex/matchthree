﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ManagerApplication : MonoBehaviour
{

    public Canvas menu = default;
    public Button btnSettings = default;
    public InputManager inputManager = default; 
    public GameObject dopPanel;
    
    private void Awake() 
    {

      //  menu.gameObject.SetActive(false);
    }

    private void Start() 
    {
        //Debug.Log((float)(Screen.height / Screen.width));
        if ((float)(Screen.height / Screen.width) == 2 || (float)(Screen.height / Screen.width) == 1.6f)
        {
            dopPanel.SetActive(true);
        }
        else
        { 
            dopPanel.SetActive(false); 
        }
        //   WindowSettings.instance.SetButtonForCall(btnSettings, btnSettings.transform.parent.parent);   
    }
}
