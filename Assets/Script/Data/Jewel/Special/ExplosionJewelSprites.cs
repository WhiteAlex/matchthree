﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
[CreateAssetMenu(menuName = "ProjectMatch3/JewelData/ExplosionJewelSprites", fileName = "ExplosionJewelSprites", order = 51)]
public class ExplosionJewelSprites : ScriptableObject
{
    [SerializeField] private List<JewelSprite> jewelSprites = new List<JewelSprite>();
    public JewelData Data;

    public JewelSprite GetSprite(JewelData.ColorJewel colorJewel)
    {
        JewelSprite jewelSprite = jewelSprites[0];
        
        foreach (var sprite in jewelSprites)
        {
            if(sprite.Color == colorJewel)
            jewelSprite = sprite;
        }

        return jewelSprite;
    }
}
