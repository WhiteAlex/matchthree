﻿using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/JewelData/CollectionJewels", fileName = "New Collection Jewels", order = 51)]
public class CollectionJewels : ScriptableObject
{
    public string Id { get => _id; }
    public CommonJewel TemplateJewelPrefab { get => _templateJewelPrefab; }
    [Header("ID Collecction")]
    private readonly string _id;

    [Header("Common Jewels")]
    [SerializeField] private List<PropertyJewel> _collectionProperty;
    [SerializeField] private JewelData _templateDataCommonJewel;
    [SerializeField] private CommonJewel _templateJewelPrefab;
    
    [Header("Bonus Jewels")]
    [SerializeField] private ExplosionJewelSprites _explosionJewelSprites;
    [SerializeField] private PropertyJewel _chameleon;
    [SerializeField] private PropertyJewel _propertyBombJewel;

    //Временный костыль
    public List<SpecialJewel> ListBonus;


    public PropertyJewel GetPropertyJewel(JewelData.ColorJewel color)
    {
        var jewelData = Instantiate(_templateDataCommonJewel);

        PropertyJewel propertyJewel = null;

        foreach (var property in _collectionProperty)
        {
            if (property.JewelSpriteData.Color == color)
            {
                jewelData.Color = property.JewelSpriteData.Color;
                propertyJewel = new PropertyJewel(property.JewelSpriteData, jewelData);
            }
        }

        return propertyJewel;
    }

    public PropertyJewel GetPropertyExplosionJewel(JewelData.ColorJewel color)
    {
        JewelSprite sprite = _explosionJewelSprites.GetSprite(color);

        JewelData data = _explosionJewelSprites.Data;
        data.Color = color;
        return new PropertyJewel(sprite, Instantiate(data));
    }

    public PropertyJewel GetPropertyChameleonJewel()
    {
        return new PropertyJewel(_chameleon);
    }

    public PropertyJewel GetPropertyBombJewel()
    {
        return new PropertyJewel(_propertyBombJewel);
    }
}