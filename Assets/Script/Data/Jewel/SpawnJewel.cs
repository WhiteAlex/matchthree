﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/JewelData/SpawnManager", fileName = "SpawnManager", order = 51)]
public class SpawnJewel : ScriptableObject
{
    [SerializeField] private List<JewelSprite> icons= new List<JewelSprite>();
    [SerializeField] private List<JewelData> data = new List<JewelData>();
}
