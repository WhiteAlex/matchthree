﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/JewelData/JewelSprite", fileName = "JewelSprite", order = 51)]
public class JewelSprite : ScriptableObject
{
    public Sprite Sprite;
    public JewelData.ColorJewel Color;
}
