﻿using UnityEngine;
[System.Serializable]
public class PropertyJewel
{
    [SerializeField] private JewelSprite _sprite;
    [SerializeField] private JewelData _data;

    public JewelSprite JewelSpriteData { get => _sprite; }
    public Sprite Sprite { get => _sprite.Sprite;}
    public JewelData Data { get => _data; set => _data = value; }
    public PropertyJewel(JewelSprite sprite, JewelData jewelData)
    {
        _sprite = sprite;
        _data = jewelData;
        _data.CurrentColor = _data.Color;
    }

    public PropertyJewel(PropertyJewel propertyJewel):
        this(propertyJewel._sprite, ScriptableObject.Instantiate(propertyJewel._data))
    {
    }
}