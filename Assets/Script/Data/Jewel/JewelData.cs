﻿using UnityEngine;
[System.Serializable, CreateAssetMenu(menuName = "ProjectMatch3/JewelData/Data", fileName = "DataJewel", order = 51)]
public class JewelData : ScriptableObject
{
    public enum ColorJewel
    {
        AntiSpecial = -9,
        AntiViolet = -8,
        AntiPink = -7,
        AntiAntiMenthol = -6,
        AntiOrange = -5,
        AntiYellow = -4,
        AntiBlue = -3,
        AntiGreen = -2,
        AntiRed = -1,
        None = 0,
        Red = 1,
        Green = 2,
        Blue = 3,
        Yellow = 4,
        Orange = 5,
        Menthol = 6,
        Pink = 7,
        Violet = 8,
        Special = 9,
    }

    public enum TypeJewel
    {
        Common,
        Special,
        Chanheless
    }

    public enum StateJewel
    {
        Normal,
        Frost,
    }

    public ColorJewel Color;
    public ColorJewel CurrentColor;
    public TypeJewel Type;
    public StateJewel state;
}
