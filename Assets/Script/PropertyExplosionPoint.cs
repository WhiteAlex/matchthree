﻿using UnityEngine;

public class PropertyExplosionPoint : MonoBehaviour
{
    [SerializeField] private Animator _animatorExplosion;
    public Animator AnimatorExplosion { private set => _animatorExplosion = value; get => _animatorExplosion;}

    private void Awake()
    {
        _animatorExplosion = GetComponent<Animator>();
    }

}
