﻿using UnityEngine;

public class Swipe : IChoose
{
    [SerializeField] private Vector2 touchBegan;
    [SerializeField] private Vector2 touchEnded;

    public Swipe()
    {
        touchBegan = Vector2.zero;
        touchEnded = Vector2.zero;
    }

    public void ChooseItems(ref Socket[] items)
    {   
        items = new Socket[] { null, null};

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
                 touchBegan = Camera.main.ScreenToWorldPoint(touch.position);

            if (touch.phase == TouchPhase.Ended)
            {
                touchEnded = Camera.main.ScreenToWorldPoint(touch.position);
                var vector = touchBegan - touchEnded;
                        
                RaycastHit2D[] hit2D = Physics2D.RaycastAll(touchBegan, -vector.normalized, 1.5f);
                
                foreach (var item in hit2D)
                {
                    if (items[0] == null)
                        items[0] = item.collider.GetComponent<Socket>();
                    else if(items[1] == null)
                        items[1] = item.collider.GetComponent<Socket>();
                    
                }
            }
        }
    }
}
