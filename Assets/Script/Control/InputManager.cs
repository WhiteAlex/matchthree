﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class InputManager
{
    [SerializeField] private Socket _selectedFirstItem;
    [SerializeField] private Socket _selectedSecondItem;
    private readonly MouseControl _mouseControl;
    private readonly Swipe _swipe;
    private IChoose _currentControl;

    [SerializeField] public static bool Focus { get; set; }

    public InputManager()
    {
        _swipe = new Swipe();
        _mouseControl = new MouseControl();
        Focus = true;

        _selectedFirstItem = null;
        _selectedSecondItem = null;
    }
    public Socket[] Pick()
    {
        Socket[] temp = new Socket[2];
        if (Focus == true)
        {
            if (CurrentSettings.instance.Control == CurrentSettings.Controls.Swipe)
            {
                _currentControl = _swipe;
            }
            else if (CurrentSettings.instance.Control == CurrentSettings.Controls.Mouse)
            {
                _currentControl = _mouseControl;
            }

            _currentControl.ChooseItems(ref temp);
            _selectedFirstItem = temp[0];
            _selectedSecondItem = temp[1];
        }

        if (_selectedFirstItem != null && _selectedSecondItem != null)
        {
           bool resultValidation = ValidateEquals() && ValidationSelected(_selectedFirstItem, _selectedSecondItem);
           if (!resultValidation)
            {
                NullSeleteds();
            }
        }

        return new Socket[] { _selectedFirstItem, _selectedSecondItem };
    }

    private bool ValidationSelected(Socket firsttItem, Socket secondItem)
    {
        Vector2 tempPos = firsttItem.Position - secondItem.Position;

        if (((Mathf.Abs(tempPos.x) == 1) && (Mathf.Abs(tempPos.y) < 1)) || ((Mathf.Abs(tempPos.x) < 1) && (Mathf.Abs(tempPos.y) == 1)))
        {
            return true;
        }
        else
        {
            return false;
        }
        
    } 

    private bool ValidateEquals() 
    {
        if (_selectedFirstItem?.Id == _selectedSecondItem?.Id)
        {
            NullSeleteds();

            return false;
        }
        else return true; 
    }

    public void NullSeleteds()
    {
        _selectedFirstItem = null;
        _selectedSecondItem = null;
    }
}
