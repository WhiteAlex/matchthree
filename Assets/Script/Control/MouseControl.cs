﻿using UnityEngine;

public class MouseControl : IChoose
{
    [SerializeField] private Socket[] _items;

    public MouseControl()
    {
        _items = new Socket[2];
    }

    public void ChooseItems(ref Socket[] items)
    {

        if(_items[0] != null && _items[1] != null) 
        { 
            _items[0] = null;
            _items[1] = null;
        }
        
        items = _items;
              
        if(Input.GetMouseButtonUp(0))
        {
            Vector2 curMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit2D = Physics2D.Raycast(curMousePos, Vector2.zero);

            if(hit2D.transform != null)
            {
                if (items[0] == null)
                {
                    if (hit2D.collider.GetComponent<Socket>())
                        items[0] = hit2D.collider.GetComponent<Socket>();
                }
                else
                {
                    if (items[1] == null)
                        items[1] = hit2D.collider.GetComponent<Socket>();
                }
            }
        }
        _items = items;
    }
}
