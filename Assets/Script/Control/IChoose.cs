﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IChoose
{
    void ChooseItems(ref Socket[] firstItem);
}
