﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class CurrentSettings : Singleton<CurrentSettings>
{
    public string Platform;
    public float VolumeBackgroundMuic;
    public float VolumeSFX;
    public Controls Control;
}
