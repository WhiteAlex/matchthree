﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Text))]
public class ViewScore : MonoBehaviour
{
    private long _score;
    private Text _text;
    private void Awake()
    {
        _text = GetComponent<Text>();
        _score = 0;
        _text.text = $"Очков: {_score}";
    }
    public void UpdateViewScore(long score)
    {
        _score += score;
        _text.text = $"Очков: {_score}";
    }
}
