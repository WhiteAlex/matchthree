﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorGameField : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Transform _gameField;
    private void Start()
    {
        if (_camera == null)
            _camera = Camera.main;
    }

    private void Update()
    {
        Vector3 temp = _camera.ScreenToWorldPoint(transform.position);
        Vector3 newCoords = _camera.WorldToScreenPoint(temp);
        _gameField.position = new Vector3(newCoords.x, newCoords.y, -1);
    }

    public bool GetPositionToScreen(Transform transform, float depth) 
    {
        if (_camera != null)
        {
            Vector3 positionInWorld = _camera.ScreenToWorldPoint(transform.position);
            Vector3 positionOnScreen = _camera.WorldToScreenPoint(positionInWorld);
            transform.position =  new Vector3(positionOnScreen.x, positionOnScreen.y, depth);
            return true;
        }

        return false;
    }
}
