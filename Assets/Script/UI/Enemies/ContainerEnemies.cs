﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ContainerEnemies : MonoBehaviour
{
    public IActionNPC CurrentEnemy { get => _currentEnemy; }
    public List<IActionNPC> Enemies { get => _enemies; }

    [SerializeField] private List<EasyEnemy> _listEnemies;
    [SerializeField] private List<IActionNPC> _enemies;
    [SerializeField] private IActionNPC _currentEnemy;
    [SerializeField] private byte _numberCurrentEnemy;
    [SerializeField] private GameField _gameField;
    private Helper _helper;

    public void Init(GameField gameField) 
    {
        _gameField = gameField;
        _helper = new Helper(_gameField);

        foreach (var enemy in _listEnemies)
        {
            enemy.Helper = _helper;
        }
    }
    private void Awake()
    {
        _currentEnemy = default;
        _enemies = new List<IActionNPC>();
        _numberCurrentEnemy = 0;

        foreach (var e in _listEnemies)
        {
            _enemies.Add(e);
        }

        if (_enemies.Count > 0)
        {
            _currentEnemy = _enemies[_numberCurrentEnemy];
        }
    }

    public void ApplyDamage(long damage)
    {
        if (_currentEnemy?.IsDead() == true)
        {
            if (_numberCurrentEnemy < _enemies.Count - 1)
            {
                _numberCurrentEnemy++;
            }
            else
                return;
     
            _currentEnemy = _enemies[_numberCurrentEnemy];
        }
        _currentEnemy.TakeDamage((int)damage);
    }
}
