﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CounterJewels
{
    public System.Action<JewelData.ColorJewel, int> Counted;
    public event System.Action<long> OnCountedScore;

    private long _score;
 
    public CounterJewels()
    {
        _score = 0;
        Counted += CountedScore;
    }

    public void Init()
    {
        _score = 0;
    }

    private void CountedScore(JewelData.ColorJewel color, int size) 
    {
        int tempScore = size;
        //if (size > 3) tempScore *= 1.3f;
        _score += size;
        //Debug.Log($"CounterJewels.Score: {_score}");
        OnCountedScore?.Invoke(size);
        
    }
}
