﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WindowMenu : MonoBehaviour
{
    [SerializeField] private Canvas window = default;
//    [SerializeField] private Button btnCallWindowSettings = default;
//    [SerializeField] private RectTransform _placeForWindowSettings = default;
    [SerializeField] private CanvasGroup _canvasGroup; 

    private void Awake() 
    {
        _canvasGroup = window.GetComponent<CanvasGroup>();
        window.enabled = false;
        window.sortingOrder = 0;

    }

    public void CloseWindowMenu()
    {
        _canvasGroup.blocksRaycasts = false;
        _canvasGroup.interactable = false;

        window.enabled = false;
        window.sortingOrder = 0;
    }

    public void OpenWindowMenu()
    {
        _canvasGroup.blocksRaycasts = true;
        _canvasGroup.interactable = true;
        window.enabled = true;
        window.sortingOrder = 6;
    }

    public void ExitGame() 
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
