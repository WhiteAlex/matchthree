﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrthographicSize: MonoBehaviour
{
    public float backgroundWidthSize; 
    public float ratio;
    public float width;
    public Camera _camera;
    private void Awake()
    {
        _camera = GetComponent<Camera>();
        if(ratio == 1.6f)
        {
            backgroundWidthSize = 800;

        }else if(System.Math.Round(ratio, 4) == 2.0556)
        {
            backgroundWidthSize = 704;
        }
        else backgroundWidthSize = 720;


    }
    
    private void Start()
    {
        _camera = Camera.main;
        ratio = (float) Screen.height / Screen.width;

        if(ratio == 1.6f)
        {
            backgroundWidthSize = 800;
        }else if(System.Math.Round(ratio, 4) == 2.0556)
        {
            backgroundWidthSize = 704;
        }
        else backgroundWidthSize = 720;
        
        width = backgroundWidthSize * ratio;
        
        float newOrtographicSize = width / 200f;

        _camera.orthographicSize = newOrtographicSize; 
    }

    private void Update() 
    {
     /*
        ratio = (float) Screen.height / Screen.width;
        if(ratio == 1.6f)
        {
            backgroundWidthSize = 800;
        } else if(System.Math.Round(ratio, 4) == 2.0556)
        {
            backgroundWidthSize = 704;
        }
        else backgroundWidthSize = 720;
        width = backgroundWidthSize * ratio;
        
        float newOrtographicSize = width / 200f;

        _camera.orthographicSize = newOrtographicSize;   
     */
    }
}
