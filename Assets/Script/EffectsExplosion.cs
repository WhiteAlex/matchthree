﻿using System.Collections.Generic;
using UnityEngine;

public class EffectsExplosion : MonoBehaviour
{
    [SerializeField] private PropertyExplosionPoint _pointExplosion = default;
    [SerializeField] private int _amountObjectExplosionPoint;

    [SerializeField] private PropertyExplosionLine _lineExplosion = default;
    [SerializeField] private int _amountObjectExplosionLine;

    private PoolObject<PropertyExplosionPoint> _poolPointExplosion;
    private PoolObject<PropertyExplosionLine>  _poolLineExplosion;


    private void Awake()
    {
        _poolPointExplosion = new PoolObject<PropertyExplosionPoint>(_pointExplosion, _amountObjectExplosionPoint, transform);
        _poolLineExplosion = new PoolObject<PropertyExplosionLine>(_lineExplosion, _amountObjectExplosionPoint, transform);
    }

    public void PlayAnimationExplosionPoint(Vector2 pos)
    {
        var point = _poolPointExplosion.GetFreeObject();
        {
                point.gameObject.transform.position = new Vector2(pos.x, -pos.y);
                point.AnimatorExplosion.Play("Explosion");
        }
    }

    public void PlayAnimationExplosionRow(Vector2 pos, GroupJewel.Direction direction)
    {
        var line = _poolLineExplosion.GetFreeObject();
        
        line.gameObject.transform.localPosition = new Vector2(pos.x - 0.03f, -pos.y);
        
        float position = 0;
        if (direction == GroupJewel.Direction.Horizontal)
        {                    
            position = (float)System.Math.Round((pos.y) / 7, 1);
            Rotation(line.gameObject, position, 90, 270);
        }
        else if (direction == GroupJewel.Direction.Vertical)
        {
            position = (float)System.Math.Round((pos.x) / 7, 1);
            Rotation(line.gameObject, position, 180, 0);
        }

        line.AnimatorExplosion.SetFloat("Position", Mathf.Abs(position)); 
    }


    public void Rotation(GameObject gObject, float pos, int angleRatote1, int angleRatote2)
    {
        if (pos > 0.4f)
            gObject.transform.localRotation = Quaternion.Euler(0, 0, angleRatote1);//270 & 90
        else 
            gObject.transform.localRotation = Quaternion.Euler(0, 0, angleRatote2);
    }

    public void OnStartExplosion(Vector2 position, GroupJewel groupJewel, JewelData.TypeJewel type)
    {
        if(type == JewelData.TypeJewel.Chanheless)
            PlayAnimationExplosionRow(position, groupJewel.DirectionGroup);
    }
}
