﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ResourcesSpells: MonoBehaviour 
{
    //public event System.Action<int> ChangeValue;
    public UnityAction<int> ChangeValue;
    [SerializeField] public Slider sliderRedResource = default;
    [SerializeField] public Slider sliderBlueResource = default;
    [SerializeField] public Slider sliderYellowResource = default;
    [SerializeField] public Slider sliderGreenResource = default;
    [SerializeField] public Button btnRedResource = default;
    [SerializeField] public Button btnBlueResource = default;
    [SerializeField] public Button btnYellowResource = default;
    [SerializeField] public Button btnGreenResource = default;
    [SerializeField] public Button[] buttonsSpells = { default, default, default, default };

    private void OnEnable()
    {
        ChangeValue += ChangedValue;
    }

    private void OnDisable()
    {
        ChangeValue -= ChangedValue;
    }

    public void Init()
    {
        ChangeValue += ChangedValue;
    }

    private void ChangedValue(int value)
    {
        sliderRedResource.value += value;
    }
    
    /*private void CheckReadyResoure(Slider resource, Button button)
    {
        if (resource.value < 1)
            button.interactable = false;
        else
        {
            resource.value = 1.0f;
            button.interactable = true;
        }
    }

    public void Use() 
    {
        Debug.Log("Bam-bam");
    }*/
}
