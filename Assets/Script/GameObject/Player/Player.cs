﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IActionNPC
{
    public InputManager Input { get => _inputManager; }

    [SerializeField] private Characteristic _templateCharacteristic;
    [SerializeField] private Characteristic _characteristic;

    [SerializeField] private InputManager _inputManager = new InputManager();
    private void Awake()
    {
        _characteristic = ScriptableObject.Instantiate(_templateCharacteristic);
    }

    public int Attack()
    {
        return _characteristic.Damage;
    }

    public void Heal(int countHealthPoint)
    {
        _characteristic.Health += countHealthPoint;
    }

    public bool IsDead()
    {
        return _characteristic.Health <= 0;
    }

    public void TakeDamage(int damagePoint)
    {
        _characteristic.Health -= damagePoint;
    }

    public void DecrementCounter()
    {
    }

    public bool IsReady()
    {
        return true;
    }

    public void MakeMove(out Socket[] sockets)
    {
        sockets = _inputManager.Pick();
    }
}
