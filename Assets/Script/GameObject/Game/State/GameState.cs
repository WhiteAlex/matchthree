﻿using UnityEngine;

[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/GameState/GameState", fileName = "Game State", order = 51)]
public class GameState : State
{
    private StateMachineGF _machineGF;
    private GameStateMachine _context;

    public GameState(StateMachine stateMachine) : base(stateMachine)
    {
    }

    public void Init(StateMachine stateMachine, StateMachineGF stateMachineGF)
    {
        base.Init(stateMachine);
        _context = (GameStateMachine) stateMachine;
        _machineGF = stateMachineGF;
        _machineGF.WaiteState.SetActiveState(true);
    }
    public override void Enter()
    {
        _context.Attackers = new QueueAttacker(_context.Player, _context.ContainerEnemies);
        _machineGF.InputState.OnClick += _context.PossibleTurnViewer.Click;
        _machineGF.ChangeState(_machineGF.CheckVoidState);

        _machineGF.Linker.Destroyer.RemovedGroup += (Color, Count) =>
        {
            if (_machineGF.GameState.Attackers.CurrnetAttacker.Equals(_machineGF.GameState.Attackers.Player))
                _machineGF.GameState.ContainerEnemies.ApplyDamage(Count);
            else
                _machineGF.GameState.Attackers.Player.TakeDamage(1);
        };

        _machineGF.Linker.Destroyer.RemovedGroup += _context.CounterJewels.Counted;
        // _context.CounterJewels.OnCountedScore += _context.ContainerEnemies.ApplyDamage;
        _context.CounterJewels.OnCountedScore += (score) => 
        {
            if(_machineGF.GameState.Attackers.CurrnetAttacker.Equals(_machineGF.GameState.Attackers.Player))
                _context.ViewScore.UpdateViewScore(score);
        };
    }

    public override void Exit()
    {
        _machineGF.Linker.Destroyer.RemovedGroup -= (Color, Count) =>
        {
            if (_machineGF.GameState.Attackers.CurrnetAttacker.Equals(_machineGF.GameState.Attackers.Player))
                _machineGF.GameState.ContainerEnemies.ApplyDamage(Count);
            else
                _machineGF.GameState.Attackers.Player.TakeDamage(1);
        };

        //_context.CounterJewels.OnCountedScore -= _context.ViewScore.UpdateViewScore;
       // _context.CounterJewels.OnCountedScore -= _context.ContainerEnemies.ApplyDamage;
        _machineGF.Linker.Destroyer.RemovedGroup -= _context.CounterJewels.Counted;
    }
    public override void UpdateState()
    {
        _machineGF.Tick();
    }
}
