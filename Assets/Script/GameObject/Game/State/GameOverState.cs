﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//No ready
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/GameState/GameOver", fileName = "GameOver State", order = 51)]
public class GameOverState : State
{
    public GameOverState(StateMachine stateMachine) : base(stateMachine)
    {

    }
    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void Init(StateMachine stateMachine)
    {
        base.Init(stateMachine);
    }

    public override void UpdateState()
    {
        base.UpdateState();
    }
}
