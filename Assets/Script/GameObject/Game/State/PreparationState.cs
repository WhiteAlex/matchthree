﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/GameState/Preparation", fileName = "Preparation State", order = 51)]
public class PreparationState : State
{
    private StateMachineGF _machineGF;
    public PreparationState(StateMachine stateMachine) : base(stateMachine)
    {
    }

    public void Init(GameStateMachine stateMachine, StateMachineGF stateMachineGF)
    {
        base.Init(stateMachine);

        _machineGF = stateMachineGF;
        _machineGF.WaiteState.SetActiveState(false);
        
    }

    public override void Enter()
    {
        var stateMachine = (GameStateMachine)_stateMachine;
        stateMachine.Status = GameStateMachine.GameStatus.Preparation;
    }

    public override void Exit()
    {
        var stateMachine = (GameStateMachine)_stateMachine;
        stateMachine.Status = GameStateMachine.GameStatus.Game;
    }
    public override void UpdateState()
    {

        var stateMachine = (GameStateMachine)_stateMachine;
        
        _machineGF.Tick();

        if(_machineGF.current is InputState)
        {
            _stateMachine.ChangeState(stateMachine.GameState);

        }
    }
}
