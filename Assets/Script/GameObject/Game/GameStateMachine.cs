﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(StateMachineGF), typeof(PossibleTurnViewer))]
public class GameStateMachine : StateMachine
{
    public enum GameStatus
    {
        Preparation,
        Game
    }

    public PreparationState PreparationState { get => _preparationState; }
    public GameState GameState { get => _gameState; }
    public GameOverState GameOverState{ get => _gameOverState; }
    public GameStatus Status { get => _gameStatus; set => _gameStatus = value; }
    public CounterJewels CounterJewels { get => _counterJewels; set => _counterJewels = value; }
    public ViewScore ViewScore { get => _viewScore;}
    public PossibleTurnViewer PossibleTurnViewer { get => _possibleTurn;}
    public ContainerEnemies ContainerEnemies { get => _containerEnemies; set => _containerEnemies = value; }
    public Player Player { get => _player; set => _player = value; }
    public QueueAttacker Attackers { get => _queueAttacker; set => _queueAttacker = value; }

    [SerializeField] private ContainerEnemies _containerEnemies;
    [SerializeField] private ViewScore _viewScore;
    [SerializeField] private Text _currentPlayer;
    [SerializeField] private Player _player;
    [SerializeField] private Slider _hpEnemy1;
    [SerializeField] private Slider _hpEnemy2;
    [SerializeField] private Slider _hpEnemy3;
    
    


    private CounterJewels _counterJewels;
    private GameStatus _gameStatus; 
    private StateMachineGF _stateMachineGF;
    private PreparationState _preparationState;
    private GameState _gameState;
    private GameOverState _gameOverState;
    private PossibleTurnViewer _possibleTurn;
    private QueueAttacker _queueAttacker;

    private void Awake()
    {
        Init();
    }
    private void Start()
    {
        ChangeState(_preparationState);
    }

    public override void Init()
    {
        _possibleTurn = GetComponent<PossibleTurnViewer>();
        _stateMachineGF = GetComponent<StateMachineGF>();
        _counterJewels = new CounterJewels();
        _preparationState = ScriptableObject.CreateInstance<PreparationState>();
        _gameState = ScriptableObject.CreateInstance<GameState>();
        _gameOverState = ScriptableObject.CreateInstance<GameOverState>();

        _stateMachineGF.Init(this);
        _containerEnemies.Init(_stateMachineGF.GameField);
        _possibleTurn.Init(_stateMachineGF.GameField);
        _preparationState.Init(this, _stateMachineGF);
        _gameState.Init(this, _stateMachineGF);
        //_gameOverState.Init();
    }

    private void Update()
    {
        if (_hpEnemy1 != null && _containerEnemies != null)
            _hpEnemy1.value = ((EasyEnemy)_containerEnemies.Enemies[0]).CurrentHP;
        if (_hpEnemy2 != null && _containerEnemies != null)
            _hpEnemy2.value = ((EasyEnemy)_containerEnemies.Enemies[1]).CurrentHP;
        if (_hpEnemy3 != null && _containerEnemies != null)
            _hpEnemy3.value = ((EasyEnemy)_containerEnemies.Enemies[2]).CurrentHP;

        Tick();
    }

    public override void Tick()
    {
        if(_queueAttacker != null)
            _currentPlayer.text = _queueAttacker.CurrnetAttacker.ToString();
        if (_currentState != null)
            _currentState.UpdateState();
    }
}
