﻿using UnityEngine;

public class ChameleonJewel : SpecialJewel, IMatch
{
    public ChameleonJewel()
    {
    }

    public ChameleonJewel(CommonJewel jewel): base(jewel)
    {
        Jewel.Matcher = this;
        Jewel.ColorChanger = this;
    }
    public override bool Matching(Jewel other)
    {
        if (other == null) return false;

        if (Jewel.Color == JewelData.ColorJewel.Special || Jewel.Color == JewelData.ColorJewel.AntiSpecial)
        {
            Jewel.Color = other.Color;
        }
        
        if (other.Type == JewelData.TypeJewel.Special)
        {
            other.Color = Jewel.Color;
        }

        if(Mathf.Abs(Jewel.Color.GetHashCode()) == Mathf.Abs(other.Color.GetHashCode()))
        {
            Jewel.CurrentColor = other.Color;
            return true;
        }

        return false;
    }

    public override void SetCommonJewel(CommonJewel jewel)
    {
        Jewel = jewel;
        Jewel.Matcher = this;
        Jewel.ColorChanger = this;
    }
}