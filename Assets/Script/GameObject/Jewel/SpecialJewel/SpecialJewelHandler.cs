﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SpecialJewelHandler
{
  //  public delegate void ExplosionStart(Vector2 position, GroupJewel group, JewelData.TypeJewel type);
    public Action<GameField, Socket, GroupJewel, List<GroupJewel>> FindSpecialJewel;
    public event Action<Socket[], List<GroupJewel>> RemovedGroup;

    public SpecialJewelHandler()
    {
        FindSpecialJewel += HandlerSpecialJewelBomb;
    }

    private void HandlerSpecialJewelBomb(GameField gameField, Socket socket, GroupJewel group, List<GroupJewel> groups)
    {
        if (socket.Jewel.Skill != null && socket.Jewel.Type == JewelData.TypeJewel.Special)
        {
            socket.Jewel.Skill.UseSpell(gameField, group.DirectionGroup, out GroupJewel groupJewel);
            
            var tempList = new List<GroupJewel>(1)
            {
                groupJewel
            };

            RemovedGroup.Invoke(new Socket[2] { null, null}, tempList);
        }
    }
}
