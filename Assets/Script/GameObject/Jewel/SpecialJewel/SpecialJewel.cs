﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enum = System.Enum;

public abstract class SpecialJewel: IChangeable
{
    public bool IsSelected { get; protected set; }

    protected CommonJewel Jewel;

    public SpecialJewel()
    {
        IsSelected = false;
    }
    public SpecialJewel(CommonJewel jewel): this()
    {
        Jewel = jewel;
    }
   
    public virtual bool Matching(Jewel other)
    {
        if (other == null) return false;

        if (Jewel.Color == JewelData.ColorJewel.Special || Jewel.Color == JewelData.ColorJewel.AntiSpecial)
            Jewel.Color = other.Color;

        if (other.Type == JewelData.TypeJewel.Special)
            other.Color = Jewel.Color;

        return Mathf.Abs(Jewel.Color.GetHashCode()) == Mathf.Abs(other.Color.GetHashCode());
    }

    public virtual void ChangeColorOnAnti()
    {
        foreach (JewelData.ColorJewel type in Enum.GetValues(typeof(JewelData.ColorJewel)))
        {
            if (Jewel.Color.GetHashCode() > 0)
            { 
                if (type.GetHashCode() == Jewel.Color.GetHashCode() * (-1)) Jewel.Color = type;
                Jewel.Color = JewelData.ColorJewel.AntiSpecial;
                IsSelected = true;
            }
        }
    }

    public abstract void SetCommonJewel(CommonJewel jewel);
}
