﻿public interface IUsable
{
   void UseSpell(GameField gameField, GroupJewel.Direction direction, out GroupJewel groupJewel);
}
