﻿using System.Collections.Generic;
using UnityEngine;
using Enum = System.Enum;

public class ExplosionJewel : SpecialJewel, IUsable, IMatch, IChangeable
{
    private GroupJewel.Direction _direction;

    public ExplosionJewel(CommonJewel jewel): base(jewel)
    {
        Jewel.ColorChanger = this;
        Jewel.Matcher = this;
        Jewel.Skill = this;
    }

    public ExplosionJewel()
    {
    }

    public void UseSpell(GameField gameField, GroupJewel.Direction direction, out GroupJewel groupJewel)
    {
        GameField field = gameField;
        Vector2Int fieldSize = gameField.Size;
        int x = (int)Jewel.PositionOnGameField.x;
        int y = (int)Jewel.PositionOnGameField.y;
        var newGroup = new GroupJewel();

        for (byte i = 0; i < fieldSize.x; i++)
        {
            if (direction == GroupJewel.Direction.Horizontal)
            {
                if(field[x, i].Jewel != null && field[x, i].Jewel.Color > JewelData.ColorJewel.None)
                {
                    field[x, i].Jewel.ReType();
                    newGroup.Add(field[x, i]);    
                }
            }
            else if (direction == GroupJewel.Direction.Vertical)
            {
                if(field[i, y].Jewel != null && field[i, y].Jewel.Color > JewelData.ColorJewel.None)
                {
                    field[i, y].Jewel.ReType();
                    newGroup.Add(field[i, y]);    
                }
            }
        }

        groupJewel = newGroup;
    }

    public new bool Matching(Jewel other)
    {
        if (other == null) return false;

        if (other.Type == JewelData.TypeJewel.Special)
        {
            other.Color = Jewel.Color;
        }

        return Mathf.Abs(Jewel.Color.GetHashCode()) == Mathf.Abs(other.Color.GetHashCode());
    }

    public override void ChangeColorOnAnti()
    {
        if (Jewel.Color.GetHashCode() > 0) 
        {
            foreach (JewelData.ColorJewel type in Enum.GetValues(typeof(JewelData.ColorJewel)))
                if (type.GetHashCode() == Jewel.Color.GetHashCode() * (-1)) Jewel.Color = type;
        }
    }

    public override void SetCommonJewel(CommonJewel jewel)
    {
        Jewel = jewel;
        Jewel.ColorChanger = this;
        Jewel.Matcher = this;
        Jewel.Skill = this;
    }
}

