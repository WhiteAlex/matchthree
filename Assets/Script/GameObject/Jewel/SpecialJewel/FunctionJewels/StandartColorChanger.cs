﻿using Enum = System.Enum;
public class StandartColorChanger : IChangeable
{
    private readonly CommonJewel _jewel;

    public StandartColorChanger(CommonJewel jewel)
    {
        _jewel = jewel;
    }

    public void ChangeColorOnAnti()
    {
        if (_jewel.Color.GetHashCode() > 0) 
        {
            foreach (JewelData.ColorJewel type in Enum.GetValues(typeof(JewelData.ColorJewel)))
                if (type.GetHashCode() == _jewel.Color.GetHashCode() * (-1)) _jewel.Color = type;
        }
    }
}
