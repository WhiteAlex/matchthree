﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpecialSpell
{ 
   void UseSpell(GameField gameField);
}
