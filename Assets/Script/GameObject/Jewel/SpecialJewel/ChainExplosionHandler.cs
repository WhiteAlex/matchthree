﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ChainExplosionHandler
{
    public event Action<Socket[], List<GroupJewel>> RemovedGroup;
    public Action<GameField, Socket, GroupJewel, List<GroupJewel>> FindExplosionJewel;
    public event Action<Vector2, GroupJewel, JewelData.TypeJewel> StartOutExplosion;
    
    private readonly Stack<GroupJewel.Direction> _chainExplosion;
    public ChainExplosionHandler()
    {
        _chainExplosion = new Stack<GroupJewel.Direction>();
        FindExplosionJewel += HandlerSpecialJewelExplosion;
    }

    private void HandlerSpecialJewelExplosion(GameField gameField, Socket socket, GroupJewel group, List<GroupJewel> groups)
    {

        if (socket.Jewel.Skill != null && socket.Jewel.Type == JewelData.TypeJewel.Chanheless)
        {
            if (group.DirectionGroup == GroupJewel.Direction.None)
            {
                if (_chainExplosion.Count != 0)
                {

                    var preExplosionDirection = _chainExplosion.Pop();

                    if (preExplosionDirection == GroupJewel.Direction.Horizontal)
                    {
                        group.DirectionGroup = GroupJewel.Direction.Vertical;
                    }
                    else if (preExplosionDirection == GroupJewel.Direction.Vertical)
                    {
                        group.DirectionGroup = GroupJewel.Direction.Horizontal;
                    }
                }
            }

            socket.Jewel.Skill.UseSpell(gameField, group.DirectionGroup, out GroupJewel groupJewel);
            var tempList = new List<GroupJewel>(1)
            {
                groupJewel
            };

            _chainExplosion.Push(group.DirectionGroup);
            
            StartOutExplosion.Invoke(socket.Jewel.PositionOnGameField, group, socket.Jewel.Type);
            RemovedGroup?.Invoke(new Socket[2]{null, null}, tempList);
        }
    }
}
