﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombJewel : ChameleonJewel, IUsable, IMatch, IChangeable
{
    public byte  AreaExplosion { get => _areaExplosion; set =>_areaExplosion = value; }
    
    private byte _areaExplosion;

    public BombJewel()
    {
        _areaExplosion = 1;
    }
    public BombJewel(byte areaExplosion, CommonJewel jewel): base(jewel)
    {
        _areaExplosion = areaExplosion;
        Jewel.Skill = this;
        Jewel.Matcher = this;
        Jewel.ColorChanger = this;
    }

    public void UseSpell(GameField gameField, GroupJewel.Direction direction, out GroupJewel groupJewel)
    {
        GameField field = gameField;
        var newGroup = new GroupJewel
        {
            DirectionGroup = GroupJewel.Direction.None
        };
        for (int i = (byte)Jewel.PositionOnGameField.x - _areaExplosion ; i <= (byte)Jewel.PositionOnGameField.x + _areaExplosion; i++)
        {
            for (int j = (byte)Jewel.PositionOnGameField.y - _areaExplosion; j <= (byte)Jewel.PositionOnGameField.y + _areaExplosion; j++)
            {
                if (field[i, j] != null && field[i, j].Jewel != null && Jewel.PositionOnGameField != new Vector2(i,j))
                {
                    if(field[i, j].Jewel.Color > 0) 
                    {
                        field[i, j].Jewel.ReType();
                        newGroup.Add(field[i, j]);
                    }
                }
            }
        }

        groupJewel = newGroup;
    }

    public override void SetCommonJewel(CommonJewel jewel)
    {
        Jewel = jewel;
        Jewel.Skill = this;
        Jewel.Matcher = this;
        Jewel.ColorChanger = this;
    }
}
