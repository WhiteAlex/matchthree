﻿using UnityEngine;
using Enum = System.Enum;
[System.Serializable]
public abstract class Jewel: MonoBehaviour
{
    public bool Show { get => SpriteRenderer.enabled; set => SpriteRenderer.enabled = value; }
    public Color ColorSprite { get => SpriteRenderer.color; set => SpriteRenderer.color = value; }
    public JewelData.ColorJewel Color { get => _propertyJewel.Data.Color; set => _propertyJewel.Data.Color = value; }
    public JewelData.ColorJewel CurrentColor { get => _propertyJewel.Data.CurrentColor; set => _propertyJewel.Data.CurrentColor = value; }
    public JewelData.TypeJewel Type { get => _propertyJewel.Data.Type; set => _propertyJewel.Data.Type = value; }
    public JewelData.StateJewel State { get => _propertyJewel.Data.state; set => _propertyJewel.Data.state = value; }
    public Vector2 PositionOnGameField;
    public PropertyJewel PropertyJewel 
    {
        get => _propertyJewel; 
        set 
        { 
            _propertyJewel = value;
            SpriteRenderer.sprite = _propertyJewel.Sprite;
        }
    }

    [SerializeField] private PropertyJewel _propertyJewel;
    [SerializeField] protected float _speedSmoothing;
   
    protected Transform Target;
    protected SpriteRenderer SpriteRenderer;

    public void ResetOnDefaultColor()
    {
        foreach (JewelData.ColorJewel type in Enum.GetValues(typeof(JewelData.ColorJewel)))
        {
            if (Color.GetHashCode() < 0)
                if (type.GetHashCode() == Color.GetHashCode() * (-1)) Color = type;
        }
    }
    public abstract void ReType();

    public abstract bool Matching(Jewel other);

    public void SetTarget(Transform target)
    {
        Target = target;
    }
}