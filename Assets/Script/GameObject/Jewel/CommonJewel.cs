﻿using UnityEngine;

public class CommonJewel : Jewel
{
    public IMatch Matcher { get => _matcher; set => _matcher = value; }
    public IUsable Skill { get => _skill; set => _skill= value; }
    public IChangeable ColorChanger { get => _colorChanger; set => _colorChanger = value; }
    private IMatch _matcher;
    private IUsable _skill;
    private IChangeable _colorChanger;

    private void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void Update()
    {
        if (Target != null)
            transform.position = 
                Vector3.MoveTowards(transform.position, Target.position,
                _speedSmoothing * Time.deltaTime);
    }

    public override bool Matching(Jewel other)
    {
        if(_matcher == null)
        {
            if (other == null) return false;

            if (other.Type == JewelData.TypeJewel.Special)
            {
                other.Color = Color;
                other.CurrentColor = Color;
            }

            return Mathf.Abs(Color.GetHashCode()) == Mathf.Abs(other.Color.GetHashCode());
        }

        return _matcher.Matching(other);
    }

    public override void ReType()
    {
        _colorChanger.ChangeColorOnAnti();
    }
}
