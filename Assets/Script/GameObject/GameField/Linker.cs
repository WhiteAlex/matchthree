﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Linker
{
    public Destroyer Destroyer { get => _destroyer; }
    public SpawnerJewels SpawnerJewel { get => _spawnerJewels; }

    private readonly Destroyer _destroyer;
    private readonly SpawnerJewels _spawnerJewels;
    private readonly ChainExplosionHandler _chainExplosionHandler;
    private readonly SpecialJewelHandler _specialJewelHandler;

    public Linker(Destroyer destroyer, 
        SpawnerJewels spawnerJewels,
        SpecialJewelHandler specialJewelHandler, 
        ChainExplosionHandler chainExplosionHandler, 
        EffectsExplosion effectsExplosion)
    {
        _destroyer = destroyer;
        _spawnerJewels = spawnerJewels;
        _specialJewelHandler = specialJewelHandler;
        _chainExplosionHandler = chainExplosionHandler;

        _destroyer.FindingGroup += (Socket socket, int sizeGroup, JewelData.ColorJewel color) => 
                                        _spawnerJewels.OnCreatedJewel(socket, sizeGroup, color);
        _destroyer.FullUpStorage += _spawnerJewels.OnSetJewels;
        _destroyer.FindSpecialJewel += _chainExplosionHandler.FindExplosionJewel;
        _destroyer.FindSpecialJewel += _specialJewelHandler.FindSpecialJewel;
  
        _chainExplosionHandler.RemovedGroup += (sockets, groups)=> _destroyer.OnRemoving(sockets, groups);
        _specialJewelHandler.RemovedGroup += (sockets, groups) => _destroyer.OnRemoving(sockets ,groups);

        _chainExplosionHandler.StartOutExplosion += effectsExplosion.OnStartExplosion;

    }


}
