﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/Swap", fileName = "Swap State", order = 51)]
public class SwapState : State
{
    private Socket[] _selectedJewels;
    public SwapState(StateMachine stateMachine): base(stateMachine)
    { 
        _selectedJewels = new Socket[2];
    }

    public override void Init(StateMachine stateMachine)
    {
        base.Init(stateMachine);
        _selectedJewels = new Socket[2];
    }

    public void SetSelectedJewels(Socket[] selectedJewels)
    {
        _selectedJewels = selectedJewels;
    }

    public override void UpdateState()
    {
        var stateMachine = (StateMachineGF) _stateMachine;
        var state = stateMachine.SearchingState;

        if (_selectedJewels[0] != null && _selectedJewels[1] != null)
            Swap(_selectedJewels[0], _selectedJewels[1]);

        if (_stateMachine.prevState is InputState)
        {
            state.SetSelectionJewel(_selectedJewels);    
            _stateMachine.ChangeState(state);
        }
        else if (_stateMachine.prevState is SearchingState)
        {
            for (byte i = 0; i < _selectedJewels.Length; i++)
                _selectedJewels[i] = null;
            _stateMachine.ChangeState(stateMachine.InputState);
        }
    }

    private void Swap(Socket first, Socket second)
    {
        CommonJewel temp;

        if (first.Jewel != null && second.Jewel != null)
        {
            temp = first.Jewel;

            first.Jewel = second.Jewel;
            first.Jewel.SetTarget(first.transform);

            second.Jewel = temp;
            second.Jewel.SetTarget(second.transform);
        }
    }
}
