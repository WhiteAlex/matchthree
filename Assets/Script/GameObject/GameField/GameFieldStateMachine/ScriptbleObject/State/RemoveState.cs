﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/RemovingState", fileName = "Removing State", order = 51)]
public class RemoveState : State
{
    private Destroyer _destroyer;
    private GameField _gameField;
    private Socket[] _selectedJewels;
    private StateMachineGF _machineGF;

    public List<GroupJewel> GroupJewels { get; set; }

    public RemoveState(StateMachine stateMachine, GameField gameField, Destroyer destroyer) : base(stateMachine)
    {
        _machineGF = (StateMachineGF) stateMachine;
        _gameField = gameField;
        _destroyer = destroyer;
    }

    public void Init(StateMachine stateMachine, GameField gameField, Destroyer destroyer)
    {
        base.Init(stateMachine);
        _machineGF = (StateMachineGF)stateMachine;
        _gameField = gameField;
        _destroyer = destroyer;
        _selectedJewels = new Socket[2];
    }

    public override void UpdateState()
    {
        var stateMachine = (StateMachineGF) _stateMachine;

        if (GroupJewels?.Count > 0)
        {
            if (_machineGF.GameState.Status == GameStateMachine.GameStatus.Game && (_selectedJewels[0] != null && _selectedJewels[1] != null))
            {
                if (_machineGF.GameState.Attackers.CurrnetAttacker != null)
                {
                    _machineGF.GameState.Attackers.DecrementCounters();
                    _machineGF.GameState.Attackers.CurrnetAttacker.Attack();
                }
            }

            _destroyer.Removing(_selectedJewels, GroupJewels);
            System.Array.Clear(_selectedJewels, 0, _selectedJewels.Length);

            _stateMachine.ChangeState(stateMachine.FillingState);
        }
        else if(GroupJewels?.Count == 0 && stateMachine.GameState.Status == GameStateMachine.GameStatus.Preparation)
        {
            _destroyer.MoveToStorage(_gameField);

            _stateMachine.ChangeState(stateMachine.InputState);
        }
    }

    public void SetSelectionJewel(Socket[] jewels)
    {
        _selectedJewels = jewels;
    }
}
