﻿public interface IState
{
    void Enter();
    void Exit();
    bool isUpdated { get; }
    void UpdateState();
    void SetActiveState(bool value);
    void Init(StateMachine stateMachine);
}
