﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State : ScriptableObject, IState
{
    public virtual void Enter() { }
    public virtual void Exit() { }

    public virtual void Init(StateMachine stateMachine) { _stateMachine = stateMachine; }

    [SerializeField] private bool isEnable = true;
    protected StateMachine _stateMachine;

    public bool isUpdated => isEnable;

    public void SetActiveState(bool value)
    {
        isEnable = value;
    }

    public State(StateMachine stateMachine)
    {
        _stateMachine = stateMachine;
    }

    public virtual void UpdateState() {}
}
