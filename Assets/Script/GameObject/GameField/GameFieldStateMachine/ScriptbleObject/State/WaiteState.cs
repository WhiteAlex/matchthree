﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/Waite", fileName = "Waite State", order = 51)]
public class WaiteState : State
{
    private GameField _gameField;
    public State nextState;

    public WaiteState(StateMachine stateMachine, GameField gameField): base(stateMachine)
    {
        _gameField = gameField;
    }

    public void Init(StateMachine stateMachine, GameField gameField)
    {
        base.Init(stateMachine);
        _gameField = gameField;
    }
    public override void UpdateState()
    {
      //  Debug.Log(this.ToString());
        bool isAllSocketsReady = true;
        if(isUpdated == true)
        {
            var field = _gameField;
            var size = _gameField.Size;
            for (int i = 0; i < size.x; i++)
                for (int j = 0; j < size.y; j++)
                {
                    if (field[i, j].IsReady == false) isAllSocketsReady = false;
                }

        }

        if (isAllSocketsReady == true) _stateMachine.ChangeState(nextState);
    }
}
