﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PossibleTurnViewer))]
public class StateMachineGF : StateMachine
{
    public GameStateMachine GameState { get => _gameStateMachine; private set => _gameStateMachine = value; }
    public FillingState FillingState { get => _fillingState; }
    public CheckVoidState CheckVoidState { get => _checkVoidState; }
    public SearchingState SearchingState { get => _searchingState; }
    public RemoveState RemoveState { get => _removeState; }
    public InputState InputState { get => _inputState; }
    public SwapState SwapState { get => _swapState; }
    public WaiteState WaiteState { get => _waiteState; }
    public Linker Linker { get => _linker; }
    public GameField GameField { get => _gameField; }

    [SerializeField] private SpawnerJewels _spawnerJewels;
    [SerializeField] private Transform _spawnerPosition;
    [SerializeField] private ListCollectionJewels _collectionJewels;
    [SerializeField] private FieldGenerator _fieldGenerator;
    [SerializeField] private EffectsExplosion _effectsExplosion;

    private Destroyer _destroyer;

    private Linker _linker;
    private GameStateMachine _gameStateMachine;

    private FillingState _fillingState;
    private SearchingState _searchingState;
    private RemoveState _removeState;
    private InputState _inputState;
    private SwapState _swapState;
    private CheckVoidState _checkVoidState;
    private WaiteState _waiteState;
    private GameField _gameField;
    private ChainExplosionHandler _explosionHandler;
    private SpecialJewelHandler _specialJewelHandler;
    
    public void Init(GameStateMachine gameStateMachine)
    {
        _gameStateMachine = gameStateMachine;
        _fieldGenerator.Transform = transform;

        _inputState = ScriptableObject.CreateInstance<InputState>();
        _searchingState = ScriptableObject.CreateInstance<SearchingState>();
        _fillingState = ScriptableObject.CreateInstance<FillingState>();
        _removeState = ScriptableObject.CreateInstance<RemoveState>();
        _waiteState = ScriptableObject.CreateInstance<WaiteState>();
        _checkVoidState = ScriptableObject.CreateInstance<CheckVoidState>();
        _swapState = ScriptableObject.CreateInstance<SwapState>();

        _spawnerJewels = new SpawnerJewels(_collectionJewels.CurrentCollection[0], _spawnerPosition);
        _gameField = new GameField(transform, _fieldGenerator.CreatorGameField);
        _destroyer = new Destroyer(_gameField);
        _explosionHandler = new ChainExplosionHandler();
        _specialJewelHandler = new SpecialJewelHandler();

        _linker = new Linker(_destroyer, _spawnerJewels, _specialJewelHandler, _explosionHandler, _effectsExplosion);

        _fillingState.Init(this, _gameField, _spawnerJewels);
        _removeState.Init(this, _gameField, _destroyer);
        _checkVoidState.Init(this, _gameField);
        _searchingState.Init(this);
        _waiteState.Init(this, _gameField);
        _inputState.Init(this);
        _swapState.Init(this);

        ChangeState(_checkVoidState);
    }

    public override void Tick()
    {
        if (_currentState != null)
            _currentState.UpdateState();
    }
}
