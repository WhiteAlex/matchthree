﻿using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/Searching", fileName = "Searching State", order = 51)]
public class SearchingState : State
{
    private Searcher _searcher;
    private Socket[] _selectedJewels;
    private StateMachineGF _machineGF;
    public SearchingState(StateMachine stateMachine) : base(stateMachine)
    {
        _machineGF = (StateMachineGF)_stateMachine;
        _selectedJewels = new Socket[2];
        _searcher = new Searcher(_machineGF.GameField);
    }

    public override void Init(StateMachine stateMachine)
    {
        base.Init(stateMachine);
        _machineGF = (StateMachineGF) stateMachine;
        _searcher = new Searcher(_machineGF.GameField);
        _selectedJewels = new Socket[2];
    }

    public override void UpdateState()
    {
        List<GroupJewel> groups = _searcher.SearchMatch();
                
        if(groups.Count > 0 || _machineGF.GameState.Status == GameStateMachine.GameStatus.Preparation)
        {
            var nextState = _machineGF.RemoveState;
            nextState.GroupJewels = groups;
            nextState.SetSelectionJewel(_selectedJewels);
            _machineGF.ChangeState(nextState);
        }
        else if(groups.Count == 0)
        {
            _machineGF.ChangeState(_machineGF.SwapState);
        }
    }

    public void SetSelectionJewel(Socket[] jewels) 
    {
        _selectedJewels = jewels;
    }
}
