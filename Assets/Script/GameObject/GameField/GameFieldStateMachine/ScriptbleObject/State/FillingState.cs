﻿using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/Filling", fileName ="Filling State", order = 51)]
public class FillingState : State
{
    private SpawnerJewels _spawnerJewels;
    private GameField _gameField;
    public FillingState(StateMachine stateMachine, GameField gameField, SpawnerJewels spawnerJewels) : base(stateMachine)
    {
        _gameField = gameField;
        _spawnerJewels = spawnerJewels;
    }

    public void Init(StateMachine stateMachine, GameField gameField, SpawnerJewels spawnerJewels)
    {
        _stateMachine = stateMachine;
        _gameField = gameField;
        _spawnerJewels = spawnerJewels;
    }

    public override void UpdateState()
    {
       // Debug.Log(this.ToString());
        var stateMachine = (StateMachineGF)_stateMachine;
        var state = stateMachine.WaiteState;
        state.nextState = stateMachine.CheckVoidState;
     
        var field = _gameField;
        var size = _gameField.Size;

        for (int i = 0; i < size.x; i++)
        {
            if (field[i, 0].Jewel == null)
            {
                if(stateMachine.GameState.Status == GameStateMachine.GameStatus.Preparation)
                {
                    field[i, 0].Jewel = _spawnerJewels.GetNewJewel(_gameField.Transform);
                    field[i, 0].Jewel.Show = false;
                }
                else if(stateMachine.GameState.Status == GameStateMachine.GameStatus.Game)
                {   
                    if(_spawnerJewels.IsStorageNotEmpty)
                        field[i, 0].Jewel = _spawnerJewels.GetJewelFromStorage(_gameField.Transform);
                    else
                        field[i, 0].Jewel = _spawnerJewels.GetNewJewel(_gameField.Transform);
                    field[i, 0].Jewel.Show = true;
                }
                
                field[i, 0].Jewel.SetTarget(field[i, 0].transform);
            }
        }

        _stateMachine.ChangeState(stateMachine.CheckVoidState);
    }
}
