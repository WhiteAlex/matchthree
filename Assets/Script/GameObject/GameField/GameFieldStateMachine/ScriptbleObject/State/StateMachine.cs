﻿using UnityEngine;

public class StateMachine: MonoBehaviour
{
    protected State _currentState;
    protected State _prevState;
    public IState prevState => _prevState;
    public IState current => _currentState;

    public virtual void Init() { }
    public virtual void ChangeState(State newState)
    {
        if (_currentState != null)
        {
            _currentState.Exit();
        }

        _prevState = _currentState;
 
        if(newState != null)
        {
            _currentState = newState;
            _currentState.Enter();
        }
    }

    public virtual void Tick()
    {
        if (_currentState != null)
            _currentState.UpdateState();
    }
}
