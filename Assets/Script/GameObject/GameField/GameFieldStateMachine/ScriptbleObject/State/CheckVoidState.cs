﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/CheckVoidState", fileName = "CheckVoid State", order = 51)]
public class CheckVoidState : State
{
    private GameField _gameField;
    public CheckVoidState(StateMachine stateMachine, GameField gameField) : base(stateMachine)
    {
        _gameField = gameField;
    }

    public void Init(StateMachine stateMachine, GameField gameField)
    {
        _stateMachine = stateMachine;
        _gameField = gameField;
    }

    public override void UpdateState()
    {
        var stateMachine = (StateMachineGF)_stateMachine;
        var state = stateMachine.WaiteState;
        bool hasVoid = false;
        var field = _gameField;
        var size = _gameField.Size;

        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                if (field[i, j].Jewel == null) hasVoid = true;
            }
        }

        if (hasVoid == true)
        {
            _stateMachine.ChangeState(stateMachine.FillingState);
        }
        else
        {
            state.nextState = stateMachine.SearchingState;
            _stateMachine.ChangeState(state);

        }
    }
}
