﻿using UnityEngine;
[CreateAssetMenu(menuName = "ProjectMatch3/StateMachine/State/InputState", fileName = "Input State", order = 51)]
public class InputState : State
{
    public event System.Action OnClick;

    private StateMachineGF _machineGF;
    
    public InputState(StateMachine stateMachine) : base(stateMachine)
    {
    }

    public override void Init(StateMachine stateMachine)
    {
        base.Init(stateMachine);
        _machineGF = (StateMachineGF)stateMachine;
    }

    public override void UpdateState()
    {
        var stateMachine = (StateMachineGF)_stateMachine;
        var state = stateMachine.SwapState;

        Socket[] selectedJewels;

        _machineGF.GameState.Attackers.DoAttack(out selectedJewels);
          
        if (selectedJewels[0] != null && selectedJewels[1] != null)
        {
            OnClick.Invoke();
            _machineGF.GameState.Attackers.Player.Input.NullSeleteds();
            state.SetSelectedJewels(selectedJewels);
            _stateMachine.ChangeState(stateMachine.SwapState);
        }

    }

}
