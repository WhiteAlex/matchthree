﻿using UnityEngine;
[RequireComponent(typeof(SpriteRenderer), typeof(BoxCollider2D))]
public class Socket : MonoBehaviour
{
    public CommonJewel Jewel { get => _jewel; set => _jewel = value; }
    public Sprite Sprite { get => _spriteRenderer.sprite; set => _spriteRenderer.sprite = value; }
    public Vector2 Id { get => _id; set => _id = value; }
    public Vector2 Position { get => _position; set => _position = value; }
    public Socket[] NearbySocket { get => _nearbySocket; set => _nearbySocket = value; }
    public Jewel GetJewel
    {
        get
        {
            if (_isBlocked == false && _isOpened == false)
                return _jewel;
            else
                return null;
        }
    }

    public bool IsBlocked
    {
        get => _isBlocked;
        set
        {
            if (value == true)
            {
                _collider.enabled = true;
                _spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                if (_jewel != null)
                    _jewel.Show = true;
            }
            else
            {
                _collider.enabled = false;
                _spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                if (_jewel != null)
                    _jewel.Show = false;
            }
        }
    }
    public bool IsOpened
    {
        get => _isOpened;
        set
        {
            if (value == true)
            {
                _collider.enabled = true;
                _spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                if (_jewel != null)
                    _jewel.Show = true;
            }
            else
            {
                _collider.enabled = false;
                _spriteRenderer.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                if (_jewel != null)
                    _jewel.Show = false;
            }
        }
    }

    public Socket PrevSocket;

    [SerializeField] private CommonJewel _jewel;
    [SerializeField] private Vector2 _position;
    [SerializeField] private Vector2 _id;

    private bool _isBlocked;
    private bool _isOpened;
    private BoxCollider2D _collider;
    private SpriteRenderer _spriteRenderer;
    private Socket[] _nearbySocket;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _collider = GetComponent<BoxCollider2D>();
        _nearbySocket = new Socket[8];
        _jewel = null;
    }

    public bool IsReady
    {
        get{
            if(_jewel != null && transform.position == Jewel.transform.position) return true;
            return false;
        }
    }

    private void Update() 
    {
        if(Jewel != null)
            Jewel.PositionOnGameField = _position;


        if (Jewel == null)
        {
            if (PrevSocket != null)
            {
                Jewel = PrevSocket?.Jewel;
                PrevSocket.Jewel = null;
                Jewel?.SetTarget(transform);
            }
        }
    }

    public void ClearNearbySocket()
    {
        foreach (var socket in _nearbySocket)
        {
            if( socket != null && socket.IsBlocked == true)
                socket.IsBlocked = false;
        }
    }

    public override string ToString()
    {
        return "Socket ("+Id.x+","+ Id.y+")";
    }

}