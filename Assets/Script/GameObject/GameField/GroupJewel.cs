﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupJewel
{
    public enum Direction
    {
        None,
        Vertical,
        Horizontal
    }

    private readonly List<Socket> _group;

    public Direction DirectionGroup { get; set; }

    public GroupJewel() 
    {
        _group = new List<Socket>();
        DirectionGroup = Direction.None;
    }
    public void Add(Socket jewel)
    {
        _group.Add(jewel);
    }

    public List<Socket> GetGroup() => _group;

}
