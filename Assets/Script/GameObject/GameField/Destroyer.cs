﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Destroyer
{
    public delegate void FindGroupaJewels(Socket socket, int sizeGroup, JewelData.ColorJewel type);
    public System.Action<Socket[], List<GroupJewel>> OnRemoving;
    public delegate void FullUp(Queue<CommonJewel> jewels);
    public event FullUp FullUpStorage;
    public event System.Action<JewelData.ColorJewel, int> RemovedGroup;
    public event FindGroupaJewels FindingGroup;
    public event System.Action<GameField, Socket, GroupJewel, List<GroupJewel>> FindSpecialJewel;
   
    private readonly GameField _gameField;

    public Destroyer(GameField gameField)
    {
        _gameField = gameField;

        FindingGroup += RemoveJewel;
        OnRemoving += Removing;
    }

    public void Removing(Socket[] selectedJewels, List<GroupJewel> groups)
    {
        var gameField = _gameField;
        
        foreach (var group in groups)
        {
            bool isFindGroup = FindActivesJewel(selectedJewels, group, out Socket socketForSpecialJewel, out int sizeGroup, out JewelData.ColorJewel color);
         
            if(group != null)
            {
                if(RemovedGroup != null)
                {
                    if(group.GetGroup().Count > 0)
                        RemovedGroup?.Invoke(group.GetGroup()[0].Jewel.CurrentColor, group.GetGroup().Count);
                }

                foreach (var socket in group.GetGroup())
                {
                    if(socket.Jewel != null)
                    {
                        FindSpecialJewel.Invoke(gameField, socket, group, groups);
                        socket.ClearNearbySocket();
                        Remove(socket);
                    }
                }
                
                group.GetGroup().Clear();
            }
            
            if (isFindGroup == true)
            {
                FindingGroup?.Invoke(socketForSpecialJewel, sizeGroup, color);
            }
        }

        System.Array.Clear(selectedJewels, 0, selectedJewels.Length);

        groups.Clear();
    }
    private void RemoveJewel(Socket socket, int sizeGroup, JewelData.ColorJewel type)
    {
        Remove(socket);
    }

    public void Remove(Socket socket, Transform transform = null)
    {
        if (socket.Jewel != null)
        {
            if(transform != null)
            {
                socket.Jewel.transform.position = transform.position;
            }
            socket.Jewel.SetTarget(null);
            socket.Jewel.gameObject.SetActive(false);
            socket.Jewel = null; 
        }
    }

    public bool FindActivesJewel(Socket[] selectedJewels, GroupJewel group, out Socket socketForSpecialJewel, out int sizeGroup, out JewelData.ColorJewel colorJewel)
    {
        const byte minSizeGroup = 3;
        if (selectedJewels != null)
        {
            for (int i = 0; i < selectedJewels.Length; i++)
            {
                foreach (var jewel in group.GetGroup())
                {
                    if (selectedJewels[i]?.Id == jewel.Id && group.GetGroup().Count > minSizeGroup)
                    {                                                
                        sizeGroup = group.GetGroup().Count;
                        jewel.Jewel.ResetOnDefaultColor();
                        colorJewel = jewel.Jewel.CurrentColor;
                        jewel.Jewel.ReType();
                       
                        socketForSpecialJewel = jewel;
                        
                        return true;
                    }
                }
            }
            
        }
        socketForSpecialJewel = null;
        sizeGroup = 0;
        colorJewel = JewelData.ColorJewel.None;
        return false;
    }

    public void MoveToStorage(GameField gameField)
    {
        var field = gameField;
        var size = gameField.Size;
        Queue<CommonJewel> storage = new Queue<CommonJewel>();
        for (int j = size.y - 1; j >= 0; j--)
        {
            for (int i = 0; i < size.x; i++)
            {
                if (field[i, j].Jewel != null)
                {
                    MovingJewel(field[i, j].Jewel, storage) ;
                    field[i, j].Jewel = null;
                }
            }
        }
        
        FullUpStorage.Invoke(storage);
    }

    private void MovingJewel(CommonJewel jewel, Queue<CommonJewel> storage)
    {
        jewel.gameObject.SetActive(false);
        storage.Enqueue(jewel);
    }

    public void Disable()
    {
        FindingGroup -= RemoveJewel;
        OnRemoving -= Removing;
    }
}