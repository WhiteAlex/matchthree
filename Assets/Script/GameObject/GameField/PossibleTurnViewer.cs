﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossibleTurnViewer : MonoBehaviour
{ 
    public System.Action Click;
    [SerializeField] private float _speed;
    [SerializeField] private GameObject _pointer;

    private List<GroupJewel> _groupJewels;
    private GroupJewel _currentGroupJewel;
    private Helper _helper;
    private short _counter;
    private Coroutine _currentCoroutine;
    public void Init(GameField gameField)
    {
        _helper = new Helper(gameField);
        _counter = -1;
        Click += DisableView;

        _pointer.SetActive(false);
    }

    public void EnableView()
    {
        _groupJewels = _helper.Find();

        if (_counter >= _groupJewels.Count - 1)
            _counter = 0;
        else _counter++;

        _currentGroupJewel = _groupJewels[_counter];

        _pointer.transform.position = _currentGroupJewel.GetGroup()[0].transform.position;

        _pointer.SetActive(true);

        if(_currentCoroutine != null)
            StopCoroutine(_currentCoroutine);

        _currentCoroutine = StartCoroutine(PlayAnimationMove(_currentGroupJewel));
    }
    private IEnumerator PlayAnimationMove(GroupJewel group)
    {
        var waitForSeconds = new WaitForSeconds(Time.deltaTime);

        Vector3 target = group.GetGroup()[1].transform.position;
        while (_pointer.transform.position != target) 
        { 
        
            if (_pointer != null)
                _pointer.transform.position = Vector3.MoveTowards(_pointer.transform.position, target, _speed * Time.deltaTime);

            if (_pointer.transform.position == group.GetGroup()[1].transform.position) 
                target = group.GetGroup()[0].transform.position;
            else if(_pointer.transform.position == group.GetGroup()[0].transform.position) 
                target = group.GetGroup()[1].transform.position;
            yield return waitForSeconds;
        }
    
    }
    private void DisableView()
    {
        _pointer.SetActive(false);

        if(_currentCoroutine != null)
            StopCoroutine(_currentCoroutine);
    }
}