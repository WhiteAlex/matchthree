﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ProjectMatch3/Spawner/ListCollection", fileName = "NewListCollection", order = 51)]
public class ListCollectionJewels : ScriptableObject
{
    public List<CollectionJewels> CurrentCollection { private set { } get => _collections; }

    [SerializeField] public List<CollectionJewels> _collections;
    [SerializeField] public int numberOfCollection;
}
