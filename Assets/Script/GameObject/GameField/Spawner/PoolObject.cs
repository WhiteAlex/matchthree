﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoolObject<T> where T: MonoBehaviour
{
    public T Prefab { get; }
    private List<T> _pool;
    private Transform _container;

    public PoolObject(T prefab, Transform container)
    {
        Prefab = prefab;
        _container = container;
    }

    public PoolObject(T prefab, int amount, Transform container): this(prefab, container)
    {
        CreatePool(amount);  
    }


    private void CreatePool(int amount)
    {
        _pool = new List<T>();

        for (int i = 0; i < amount; i++)
        {
            _pool.Add(CreateObject());
        }
    }

    private T CreateObject(bool isActive = false)
    {
        var createdObjet = Object.Instantiate(Prefab, _container);

        createdObjet.gameObject.SetActive(isActive);
        _pool.Add(createdObjet);
        return createdObjet;
    }

    public bool HasFreeObject(out T freeObjecct)
    {
        foreach (var obj in _pool)
        {
            if(obj.gameObject.activeSelf == false)
            {
                freeObjecct = obj;
                obj.gameObject.SetActive(true);
                return true;
            }
        }
        
        freeObjecct = null;
        return false;
    }

    public T GetFreeObject()
    {
        if(HasFreeObject(out var obj) == true)
        {
            return obj;
        }
        return CreateObject(true);
    }
}
