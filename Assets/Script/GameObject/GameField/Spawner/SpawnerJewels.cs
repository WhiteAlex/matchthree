﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnerJewels
{
    public bool IsStorageNotEmpty { get => _storageJewel.Count > 0; }
    public Action<Socket, int, JewelData.ColorJewel> OnCreatedJewel;

    [SerializeField] private uint _amountJewelsInGame = 5;
    [SerializeField] private int _amountJewelsInPool = 49;
    
    private Queue<CommonJewel> _storageJewel;
    private readonly PoolObject<CommonJewel> _poolJewel;
    private readonly Transform _transformPosition;
    private readonly CommonJewel _templateJewelsPrefab;

    private readonly CollectionJewels _currentCollection;
    public SpawnerJewels(CollectionJewels collectionJewels, Transform transform)
    {
        _transformPosition = transform;
        _currentCollection = collectionJewels;

        _templateJewelsPrefab = _currentCollection.TemplateJewelPrefab;
        _poolJewel = new PoolObject<CommonJewel>(_templateJewelsPrefab, _amountJewelsInPool, _transformPosition);
        _storageJewel = new Queue<CommonJewel>();
        
        OnCreatedJewel += CreateChameleonJewel;
        OnCreatedJewel += CreateExplosionJewel;
        OnCreatedJewel += CreateBombJewel;
    }

    public void OnSetJewels(Queue<CommonJewel> jewels)
    {   
        _storageJewel = jewels; 
    }
    public CommonJewel GetNewJewel(Transform transformGameField)
    {
        CommonJewel jewel = _poolJewel.GetFreeObject();

        jewel.ColorChanger = null;
        jewel.Matcher = null;
        jewel.Skill = null;

        jewel.gameObject.transform.position = _transformPosition.position;
        jewel.PropertyJewel = _currentCollection.GetPropertyJewel((JewelData.ColorJewel)UnityEngine.Random.Range(1, (int)_amountJewelsInGame + 1));
        jewel.gameObject.name = jewel.PropertyJewel.Sprite.name;
        jewel.ColorChanger = new StandartColorChanger(jewel);

        return jewel;
    }

    public CommonJewel GetJewelFromStorage(Transform transformGameField)
    {
        CommonJewel jewel = _poolJewel.GetFreeObject();

        jewel = _storageJewel.Dequeue();
        jewel.transform.position = _transformPosition.position;
        jewel.transform.parent = _transformPosition;
        return jewel;
    }
    private CommonJewel CreateJewel<T>(PropertyJewel propertyJewel, Transform transform) where T : SpecialJewel, new()
    {
        CommonJewel jewel = _poolJewel.GetFreeObject();
        T specialProperty = new T();
        specialProperty.SetCommonJewel(jewel);
        jewel.PropertyJewel = propertyJewel;
        jewel.gameObject.transform.position = transform.position;
        jewel.gameObject.name = propertyJewel.Sprite.name;
        jewel.SetTarget(transform);
        return jewel;
    }

    private void CreateChameleonJewel(Socket socket, int sizeGroup, JewelData.ColorJewel type = JewelData.ColorJewel.None)
    {
        if (sizeGroup == 5)
        {
            socket.Jewel = CreateJewel<ChameleonJewel>(_currentCollection.GetPropertyChameleonJewel(), socket.transform);
        }
    }


    private void CreateExplosionJewel(Socket socket, int sizeGroup, JewelData.ColorJewel type = JewelData.ColorJewel.None)
    {
        if (sizeGroup == 6)
        {
            socket.Jewel = CreateJewel<ExplosionJewel>(_currentCollection.GetPropertyExplosionJewel(type), socket.transform);
        }
    }

    private void CreateBombJewel(Socket socket, int sizeGroup, JewelData.ColorJewel type = JewelData.ColorJewel.None)
    {
        if (sizeGroup == 4)
        {
            socket.Jewel = CreateJewel<BombJewel>(_currentCollection.GetPropertyBombJewel(), socket.transform); 
        }
    }

    public void Disable()
    {
        OnCreatedJewel -= CreateChameleonJewel;
        OnCreatedJewel -= CreateExplosionJewel;
        OnCreatedJewel -= CreateBombJewel;
    }
}