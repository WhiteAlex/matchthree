﻿using System.Collections.Generic;
using UnityEngine;

public class Searcher
{
    public bool HasMatch { get => _listGroups.Count > 0; }
    public List<GroupJewel> GetGroupJewels { get => _listGroups; }
    public List<GroupJewel> ListGroup { get => _listGroups; }
    
    private readonly GameField _gameField;
    private List<GroupJewel> _listGroups;
    private Vector2Int _size;

    public Searcher(GameField gameField)
    {
        _gameField = gameField;
        _size = gameField.Size;
        _listGroups = new List<GroupJewel>();

    }
    public List<GroupJewel> SearchMatch()
    {
        _listGroups.Clear();
        byte amountMatch;
        byte amountGroup = 0;

        for (byte j = 0; j < _size.y; j++)
        {
            amountMatch = 0;

            for (byte indexCol = 0; indexCol < _size.x - 1; indexCol++)
            {
                if (_gameField[indexCol, j]?.IsBlocked != true && _gameField[indexCol + 1, j]?.IsBlocked != true)
                {
                    if (_gameField[indexCol, j].Jewel.Matching(_gameField[indexCol + 1, j].Jewel))
                        amountMatch++;
                    else
                    {
                        if (amountMatch > 1)
                        {
                            ChangeSign((byte)(indexCol - amountMatch), j, amountMatch, false);

                            amountGroup++;
                        }
                        else
                        {
                            for (byte x = 0; x <= amountMatch; x++)
                            {
                                if (_gameField[(indexCol - amountMatch) + x, j]?.Jewel.Type == JewelData.TypeJewel.Special)
                                {
                                    _gameField[(indexCol - amountMatch) + x, j].Jewel.Color = JewelData.ColorJewel.Special;
                                }
                            }
                            if (_gameField[indexCol, j].Jewel.Type == JewelData.TypeJewel.Special) indexCol--;
                        }
                        amountMatch = 0;
                    }
                }
                else
                {
                    if (_gameField[indexCol, j]?.IsBlocked != true && _gameField[indexCol + 1, j]?.IsBlocked == true)
                    {

                        if (indexCol - 1 > 0)
                            if (_gameField[indexCol - 1, j].Jewel.Matching(_gameField[indexCol, j].Jewel))
                                amountMatch++;
                    }
                }
            }

            if (amountMatch > 1)
            {
                ChangeSign((byte)(_size.x - 1 - amountMatch), j, amountMatch, false);
                amountGroup++;
            }
            else
            {
                for (int x = 0; x <= amountMatch; x++)
                {
                    if (_gameField[(_size.x - 1 - amountMatch) + x, j]?.Jewel.Type == JewelData.TypeJewel.Special)
                        _gameField[(_size.x - 1 - amountMatch) + x, j].Jewel.Color = JewelData.ColorJewel.Special;
                }

            }
        }

        for (byte indexCol = 0; indexCol < _size.x; indexCol++)
        {
            amountMatch = 0;
            for (byte indexRow = 0; indexRow < _size.y - 1; indexRow++)
            {
                if (_gameField[indexCol, indexRow]?.IsBlocked == false && _gameField[indexCol, indexRow + 1]?.IsBlocked == false)
                {
                    if (_gameField[indexCol, indexRow].Jewel.Matching(_gameField[indexCol, indexRow + 1].Jewel))
                    {
                        amountMatch++;
                    }
                    else
                    {
                        if (amountMatch > 1)
                        {
                            ChangeSign(indexCol, (byte)(indexRow - amountMatch), amountMatch, true);

                            amountGroup++;
                        }
                        else
                        {
                            for (byte x = 0; x <= amountMatch; x++)
                            {
                                if (_gameField[indexCol, (indexRow - amountMatch) + x]?.Jewel.Type == JewelData.TypeJewel.Special)
                                {
                                    _gameField[indexCol, (indexRow - amountMatch) + x].Jewel.Color = JewelData.ColorJewel.Special;
                                }
                            }

                            if (_gameField[indexCol, indexRow].Jewel.Type == JewelData.TypeJewel.Special) indexRow--;
                        }
                        amountMatch = 0;
                    }
                }
                else
                {
                    if (_gameField[indexCol, indexRow]?.IsBlocked != true && _gameField[indexCol, indexRow + 1]?.IsBlocked == true)
                    {
                        if (indexRow - 1 > 0)
                            if (_gameField[indexCol, indexRow - 1].Jewel.Matching(_gameField[indexCol, indexRow].Jewel))
                                amountMatch++;
                            else
                            {
                                if (_gameField[indexCol, indexRow - 1]?.Jewel.Type == JewelData.TypeJewel.Special)
                                {
                                    _gameField[indexCol, indexRow - 1].Jewel.Color = JewelData.ColorJewel.Special;
                                }
                                if (_gameField[indexCol, indexRow - 1].Jewel.Type == JewelData.TypeJewel.Special) indexRow--;
                            }
                    }
                }
            }

            if (amountMatch > 1)
            {
                ChangeSign(indexCol, (byte)(_size.y - 1 - amountMatch), amountMatch, true);

                amountGroup++;
            }
            else
            {
                for (byte x = 0; x <= amountMatch; x++)
                {
                    if (_gameField[indexCol, (_size.y - 1 - amountMatch) + x]?.Jewel.Type == JewelData.TypeJewel.Special)
                    {
                        _gameField[indexCol, (_size.y - 1 - amountMatch) + x].Jewel.Color = JewelData.ColorJewel.Special;
                    }
                }
            }
        }
        FindBigGroup();
        return _listGroups;
    }

    private void ChangeSign(byte startX, byte startY, byte amount, bool isVertical = false)
    {

        if (isVertical == false)
        {
            var group = new GroupJewel
            {
                DirectionGroup = GroupJewel.Direction.Horizontal
            };

            for (byte x = 0; x <= amount; x++)
            {

                if (_gameField[startX + x, startY].GetJewel != null && _gameField[startX + x, startY].Jewel.Color.GetHashCode() > 0)
                {
                    _gameField[startX + x, startY].Jewel.ReType();
                }
                
                group.Add(_gameField[startX + x, startY]);
            }

            if(group.GetGroup().Count > 0)
                _listGroups.Add(group);
        }

        if (isVertical == true)
        {
            var group = new GroupJewel
            {
                DirectionGroup = GroupJewel.Direction.Vertical
            };


            for (byte x = 0; x <= amount; x++)
            {
                if (_gameField[startX, startY + x].GetJewel != null && _gameField[startX, startY + x].Jewel.Color.GetHashCode() > 0)
                {
                    _gameField[startX, startY + x].Jewel.ReType();    
                }

                group.Add(_gameField[startX, startY + x]);
                
            }

            if (group.GetGroup().Count > 0)
                _listGroups.Add(group);
        }

    }
     
    /*public void SetSelectionJewel(ref Socket[] jewels)
    {
        _jewels = jewels;
    }*/
    /*
    public void FindActivesJewel()
    {
        
     //   GatheringSelectedJewel();

        if (_jewels != null)
        {
            for (int i = 0; i < _jewels.Length; i++)
            {
                foreach (var group in _listGroups)
                {
                    foreach (var jewel in group.GetGroup())
                    {
                        if (_jewels[i]?.Id == jewel.Id && group.GetGroup().Count > 3)
                        {
                            OnFindGroup?.Invoke(jewel, group, jewel.jewel.Color);
                            group.GetGroup().Remove(jewel);
                            break;
                        }
                    }
                }
            }
            _jewels = new Socket[] { null, null };
            //_listGroups.Clear();
        }
    }*/
    
    public void GatheringSelectedJewel()
    {
        for (byte i = 0; i < _gameField.Size.x; i++)
        {
            for (byte j = 0; j < _gameField.Size.y; j++)
            {
                if(_gameField[i,j].Jewel.Color.GetHashCode() < 0)
                {
                    bool match = false;
                    foreach (var group in _listGroups)
                    {
                        if (group.GetGroup().Exists(x => x == _gameField[i, j]) == true) 
                        {
                            match = true;
                        }
                    }

                    if (match == false)
                    {
                        var groupJewel = new GroupJewel();
                        groupJewel.Add(_gameField[i, j]);
                    }
                }
            }
        }
    }

    private void FindBigGroup()
    {
        List<Socket> groupJewel = null;
        for (byte i = 0; i < _listGroups.Count; i++)
        {
            for(byte j = 0; j < _listGroups.Count; j++)
            {
                if(i != j)
                {
                    if (_listGroups[i] != null) 
                    { 
                        foreach (var jewelA in _listGroups[i].GetGroup())
                        {
                            if(_listGroups[j] != null)
                            {
                                 foreach (var jewelB in _listGroups[j].GetGroup())
                                 {                       
                                     if (jewelB != null && (jewelA.Id == jewelB.Id))
                                     {
                                        groupJewel = _listGroups[j].GetGroup();
                                       
                                        _listGroups[j] = null;
                                       
                                        break;
                                     }
                                 }
                            }
                        }

                        if (groupJewel != null)
                        {
                            foreach (var jewel in groupJewel)
                            {
                                _listGroups[i].GetGroup().Add(jewel);
                            }

                            groupJewel.Clear();
                            groupJewel = null;
                         
                        }
                    }
                }
            }
        }

        List<GroupJewel> groups = new List<GroupJewel>();
        foreach (var group in _listGroups)
        {
            if (group != null)
                groups.Add(group);
        }

         _listGroups = groups;
        RemoveDuplicate();
    }

    private void RemoveDuplicate()
    {
        foreach (var group in _listGroups)
        {
            for (var indexFirstJewel = 0; indexFirstJewel < group.GetGroup().Count; indexFirstJewel++)
            {
                if(group.GetGroup()[indexFirstJewel] != null)
                {
                    for (var indexSecondJewel = 0; indexSecondJewel < group.GetGroup().Count; indexSecondJewel++)
                    {
                        if (indexFirstJewel != indexSecondJewel)
                        {
                            if(group.GetGroup()[indexSecondJewel] != null)
                            {
                                if (group.GetGroup()[indexFirstJewel].Id == group.GetGroup()[indexSecondJewel].Id)
                                group.GetGroup().Remove(group.GetGroup()[indexSecondJewel]);
                            }
                          
                        }
                    
                    }
                }
            }
        }
    }
}
