﻿using System.Collections.Generic;
using UnityEngine;
using Enum = System.Enum;

public class Helper
{
    private struct Mask
    {
        public int AmountRows { get => _mask.GetUpperBound(0) + 1; }
        public int AmountColumns { get => _mask.GetUpperBound(1) + 1; }

        public int this[int indexRows, int indexColumns]
        {
            get
            {
                return _mask[indexRows, indexColumns];
            }
        }

        private readonly int[,] _mask;
        private readonly Vector2Int[] _selectedElements;

        public Mask(int[,] mask, Vector2Int[] selectedElements)
        {
            _mask = mask;
            _selectedElements = selectedElements;
        }

        public void ChangeColor(JewelData.ColorJewel colorJewel)
        {
            for (int indexRow = 0; indexRow < AmountRows; indexRow++)
            {
                for (int indexColumn = 0; indexColumn < AmountColumns; indexColumn++)
                {
                    if (_mask[indexRow, indexColumn] != 0)
                    {
                        _mask[indexRow, indexColumn] = (int)colorJewel;
                    }
                }
            }
        }

        public bool ComparePosition(Vector2Int position)
        {
            foreach (var element in _selectedElements)
            {
                if (element == position)
                    return true;
            }
            return false;
        }
    }

    private readonly Mask[] _masks;
    private readonly GameField _gameField;

    public Helper()
    {
        _masks = new Mask[8]
        {
            new Mask(new int[,] { { 1, 0, 1, 1 } }, new Vector2Int[] { new Vector2Int(0, 0), new Vector2Int(0, 1) }),
            new Mask(new int[,] { { 1, 1, 0, 1 } }, new Vector2Int[] { new Vector2Int(0, 3), new Vector2Int(0, 2) }),
            new Mask(new int[,] { { 1, 0, 0 }, { 0, 1, 1 } }, new Vector2Int[] { new Vector2Int(0, 0), new Vector2Int(1, 0) }),
            new Mask(new int[,] { { 0, 1, 1 }, { 1, 0, 0 } }, new Vector2Int[] { new Vector2Int(0, 0), new Vector2Int(1, 0) }),
            new Mask(new int[,] { { 1, 1, 0 }, { 0, 0, 1 } }, new Vector2Int[] { new Vector2Int(0, 2), new Vector2Int(1, 2) }),
            new Mask(new int[,] { { 0, 0, 1 }, { 1, 1, 0 } }, new Vector2Int[] { new Vector2Int(0, 2), new Vector2Int(1, 2) }),
            new Mask(new int[,] { { 1, 0, 1 }, { 0, 1, 0 } }, new Vector2Int[] { new Vector2Int(0, 1), new Vector2Int(1, 1) }),
            new Mask(new int[,] { { 0, 1, 0 }, { 1, 0, 1 } }, new Vector2Int[] { new Vector2Int(0, 1), new Vector2Int(1, 1) })
        };
    }
    public Helper(GameField gameField):this()
    {
        _gameField = gameField;
    }
    
    public List<GroupJewel> Find()
    {
        List<GroupJewel> listGroup = new List<GroupJewel>();

        foreach (JewelData.ColorJewel type in Enum.GetValues(typeof(JewelData.ColorJewel)))
        {
            if(((int)type) > 0)
            {
                foreach(Mask mask in _masks)
                {
                    mask.ChangeColor(type);
                    for(int indexRows = 0; indexRows < _gameField.Size.x; indexRows++)
                    {
                        for (int indexColumns = 0; indexColumns < _gameField.Size.y; indexColumns++)
                        {
                           var groupJewel = new GroupJewel 
                           { 
                                DirectionGroup = GroupJewel.Direction.Vertical
                           };
                           
                            if (indexRows + (mask.AmountRows - 1) < _gameField.Size.x && indexColumns + (mask.AmountColumns - 1) < _gameField.Size.y)
                            {
                                ExecuteActionOnVertical(mask, indexRows, indexColumns, out int amountMatch, (socket) => { });

                                if (amountMatch > 2)
                                {
                                    ExecuteActionOnVertical(mask, indexRows, indexColumns, out amountMatch,
                                        (socket) =>
                                        {
                                            groupJewel.Add(socket);
                                        });

                                    listGroup.Add(groupJewel);
                                }
                            }


                            groupJewel = new GroupJewel
                            {
                                DirectionGroup = GroupJewel.Direction.Horizontal
                            };

                            if (indexRows + (mask.AmountColumns - 1) < _gameField.Size.y && indexColumns + (mask.AmountRows - 1) < _gameField.Size.x)
                            {
                                ExecuteActionOnHorizontal(mask, indexRows, indexColumns, out int amountMatch,(socket) => {});

                                if (amountMatch > 2)
                                {
                                    ExecuteActionOnHorizontal(mask, indexRows, indexColumns,out amountMatch, 
                                        (socket) => 
                                        {
                                            groupJewel.Add(socket);
                                        });

                                    listGroup.Add(groupJewel);
                                }
                            }
                        }
                    }
                }
            }
        }

        return listGroup;
    }

    private void ExecuteActionOnVertical(Mask mask, int indexRows, int indexColumns, out int amountMatch, System.Action<Socket> action)
    {
        int counterMatch = 0;
        for (int indexRowMask = 0; indexRowMask < mask.AmountRows; indexRowMask++)
        {
            for (int indexColumnMask = 0; indexColumnMask < mask.AmountColumns; indexColumnMask++)
            {
                if (mask[indexRowMask, indexColumnMask] == (int)_gameField[indexRows + indexRowMask, indexColumns + indexColumnMask].Jewel.Color)
                {
                    counterMatch++;
                }
                
                if (mask.ComparePosition(new Vector2Int(indexRowMask, indexColumnMask)) == true)
                    action.Invoke(_gameField[indexRows + indexRowMask, indexColumns + indexColumnMask]);
            }
        }

        amountMatch = counterMatch;
    }

    private void ExecuteActionOnHorizontal(Mask mask, int indexRows, int indexColumns, out int amountMatch, System.Action<Socket> action)
    {
        int counterMatch = 0;
        for (int indexColumnMask = 0; indexColumnMask < mask.AmountColumns; indexColumnMask++)
        {
            for (int indexRowMask = 0; indexRowMask < mask.AmountRows; indexRowMask++)
            {
                if (mask[indexRowMask, indexColumnMask] == (int)_gameField[indexRows + indexColumnMask, indexColumns + indexRowMask].Jewel.Color)
                {
                    counterMatch++;
                }

                if(mask.ComparePosition(new Vector2Int(indexRowMask, indexColumnMask)) == true)
                    action.Invoke(_gameField[indexRows + indexColumnMask, indexColumns + indexRowMask]);
            }
        }

        amountMatch = counterMatch;
    }

}
