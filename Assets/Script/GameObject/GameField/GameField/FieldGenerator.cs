﻿using System.Collections.Generic;
using UnityEngine;
[System.Serializable, CreateAssetMenu(menuName = "ProjectMatch3/GameFieldData/GeneratorGameField", fileName = "NewGeneratorGameField", order = 51)]
public class FieldGenerator: ScriptableObject
{
    [SerializeField] private List<TemplateCreatingGameField> _creatorGameFields;
    [SerializeField] private Transform _transform;
    [SerializeField] private int _idSelectedTemplate;

    public Transform Transform{ get => _transform; set => _transform = value; }
    public int IdSelectedTemplate { get => _idSelectedTemplate; set => _idSelectedTemplate = value; }
    public Socket[,] CreatorGameField { get => _creatorGameFields[_idSelectedTemplate].CreateGameField(_transform); }
}
