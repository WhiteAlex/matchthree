﻿using UnityEngine;

public class GameField
{
    public Vector2Int Size { get => _size; }
    public Transform Transform { get => _transform; }

    private readonly Socket[,] _gameField;
    private Vector2Int _size;
    private readonly Transform _transform;

    public Socket this[int indexI,int indexJ]
    {
        set 
        {
            if(indexI >= 0 && indexI < _size.x && indexJ >= 0 && indexJ < _size.y)
            _gameField[indexI, indexJ] = value;
        }

        get 
        {
            if (indexI >= 0 && indexI < _size.x && indexJ >= 0 && indexJ < _size.y)
                return _gameField[indexI, indexJ];
            else 
                return null;
        }
    }

    public GameField(Transform transform, Socket[,] template)
    {
        _transform = transform;
        _size.x = template.GetLength(0);
        _size.y = template.GetLength(1);
        _gameField = template;

        CreateRelationsBetween();
    } 

    private void CreateRelationsBetween()
    {
        for (int i = 0; i < _size.x; i++)
        {
            for (int j = 0; j < _size.y; j++)
            {
                if(j < _size.y - 1)
                    _gameField[i, j + 1].PrevSocket = _gameField[i, j];
                //_gameField[i, j + 1].PrevSocket = _gameField[i, j];
                int numberNearbySocket = 0;
                for (int indexNearbySocketTop = -1; indexNearbySocketTop < 2; indexNearbySocketTop++)
                {
                    for(int indexNearbySocketLeft = -1; indexNearbySocketLeft < 2; indexNearbySocketLeft++)
                    {
                        int boundTop = j + indexNearbySocketTop;
                        int boundLeft = i + indexNearbySocketLeft;
                        if ((boundTop >= 0 && boundTop < _size.y) && (boundLeft >= 0 && boundLeft < _size.x))
                        {
                            if (!_gameField[j + indexNearbySocketTop, i + indexNearbySocketLeft].Equals(_gameField[j, i]))
                            {
                                _gameField[j, i].NearbySocket[numberNearbySocket] = _gameField[j + indexNearbySocketTop, i + indexNearbySocketLeft];
                                numberNearbySocket++;
                            }
                        }
                        

                    }
                }
            }
        }
    }

    public void Visible(bool value)
    {
        for (int i = 0; i < _size.x; i++)
            for (int j = 0; j < _size.y; j++)
            {
                if (_gameField[i, j].Jewel != null)
                    _gameField[i, j].Jewel.Show = value;
            }
    }
}
