﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class TemplateCreatingGameField : ScriptableObject, ICreatorGameField
{
    [SerializeField] protected GameFieldData _data;
    
    protected Socket[,] _gameField;
    public abstract Socket[,] CreateGameField(Transform transform);

}
