﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, 
CreateAssetMenu(menuName = "ProjectMatch3/GameFieldData/Data", fileName = "NewGameFieldData", order = 51)]
public class GameFieldData: ScriptableObject
{
    [SerializeField] private List<Sprite> _sprites;
    [SerializeField] private Socket _templateSocket;
    [SerializeField] private Vector2Int _size;
    [SerializeField] private Vector2 _offset;

    public List<Sprite> Sprites { get => _sprites; set => _sprites = value; }
    public Socket TemplateSocket { get => _templateSocket; set => _templateSocket = value; }
    public Vector2Int Size { get => _size; set => _size = value; }
    public Vector2 Offset { get => _offset; set => _offset = value; }
}
