﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable, CreateAssetMenu(menuName = "ProjectMatch3/GameFieldData/Template/Common", fileName = "CommonTemplate", order = 51)]

public class CommonTemplateGameField : TemplateCreatingGameField
{
    public override Socket[,] CreateGameField(Transform transform)
    {
        _gameField = new Socket[_data.Size.x, _data.Size.y];
        int[] angle = new int[4] { 0, 90, 180, 270 };
        for (int i = 0; i < _data.Size.x; i++)
        {
            Sprite sprite;
            Vector3 position = new Vector3();
            for (int j = 0; j < _data.Size.y; j++)
            {
                position = new Vector3(transform.position.x + (i * _data.Offset.x), 
                                        transform.position.y - (j * _data.Offset.y), -2);

                if ((j + i % 2) % 2 == 0)
                {
                    sprite = _data.Sprites[0];
                }
                else
                {
                    sprite = _data.Sprites[1];
                }

                _gameField[i, j] = Object.Instantiate<Socket>(_data.TemplateSocket, position, Quaternion.Euler(0,0,angle[Random.Range(0, 3)])
                              , transform);
                _gameField[i, j].Sprite = sprite;
                _gameField[i, j].IsBlocked = false;
                _gameField[i, j].IsOpened = true;
                _gameField[i, j].name = "(" + i + "," + j + ")";
                _gameField[i, j].Position = new Vector2(i, j);
                _gameField[i, j].Id = new Vector2(i, j);
            }
        }

        return _gameField;
    }
}

//Quaternion.AxisAngle(position + , Mathf.Deg2Rad * angle[Random.Range(0, 3)])