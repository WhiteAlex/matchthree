﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class EasyEnemy : Enemy, IActionNPC
{
    public int Attack()
    {
        if (IsReady() == true)
        { 
            Characteristic.ResetCounterLead();
            return Characteristic.Damage; 
        }
        return 0;
    }

    public void TakeDamage(int damage)
    {
        if(IsDead() != true)
            Characteristic.Health -= damage;
    }

    public void Heal(int countHealthPoint)
    {
        Characteristic.Health += 0;
    }

    public bool IsDead()
    {
        if(Characteristic.Health <= 0)
        {
            Sprite.color = new Color(1f, 1f, 1f, 0f);
            return true;
        }
        return false;
    }

    public void DecrementCounter()
    {
        if (Characteristic.CounterLead > 0 && IsDead() == false)
            Characteristic.CounterLead--;
    }

    public bool IsReady()
    {
        return (Characteristic.CounterLead == 0 && IsDead() == false);
    }
}
