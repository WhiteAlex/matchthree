﻿using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public GameField GameField { get => _gameField; set => _gameField = value; }
    public ISkill CommonSkill{ get => _commonSkill; set => _commonSkill = value; }
    public ISkill SpecialSkill{ get => _specialSkill; set => _specialSkill = value; }
    public Characteristic Сharacteristic { get => _characteristic; }
    public float CurrentHP { get => _characteristic.Health / _characteristic.MaxHealthPoint; }
    public Image Sprite { get => _sprite; }
    public Helper Helper { set => _helper = value; }
    protected Characteristic Characteristic { get => _characteristic; set => _characteristic = value; }

    private GameField _gameField;

    private Helper _helper;
    private Characteristic _characteristic;
    private Image _sprite;
    private ISkill _commonSkill;
    private ISkill _specialSkill;
    
    [SerializeField] private Characteristic _tamplateCharacteristic;
    private void Awake()
    {
        _characteristic = ScriptableObject.Instantiate(_tamplateCharacteristic);

        _sprite = GetComponent<Image>();
    }

    public void MakeMove(out Socket[] sockets)
    {
        var listTurns = _helper.Find();

        int numPair = Random.Range(0, listTurns.Count - 1);

        Socket[] pairs = new Socket[2] { listTurns[numPair].GetGroup()[0], listTurns[numPair].GetGroup()[1] };

        sockets = pairs;
    }

    public void UseSkills()
    {
      //  _commonSkill.UseSkill();
    }
}
