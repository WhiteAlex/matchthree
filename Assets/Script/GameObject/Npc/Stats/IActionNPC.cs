﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActionNPC
{
    int Attack();
    void TakeDamage(int damagePoint);
    void Heal(int countHealthPoint);
    bool IsDead();
    void DecrementCounter();
    bool IsReady();
    void MakeMove(out Socket[] sockets);
}
