﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ProjectMatch3/Characters/Сharacteristic", fileName = "New characteristic", order = 51)]
public class Characteristic: ScriptableObject
{
    public string Name;
    public float Health;
    public float MaxHealthPoint;

    public int Damage
    {
        get => _damage;
        set
        {
            if (value < 0)
            {
                _damage = 0;
            }

            _damage = value;
        }
    }

    public int Speed
    {
        get => _speed;
        set
        {
            if (value < 0)
            {
                _speed = 0;
            }

            _speed = value;
        }
    }


    public int CounterLead
    { 
        get => _counterLead; 
        set 
        {
            if (value > Speed)
            {
                _counterLead = Speed;
            }
            else if (value < 0) 
            { 
                _counterLead = _speed; 
            }
            _counterLead = value;
        }
    }
    
    [SerializeField]private int _speed;
    [SerializeField]private int _counterLead;
    [SerializeField]private int _damage;

    public void ResetCounterLead()
    {
        _counterLead = _speed;
    }

}

