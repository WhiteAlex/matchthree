﻿using System.Collections.Generic;
using UnityEngine;

public class QueueAttacker
{
    public Player Player { get => _player; }
    public ContainerEnemies ContainerEnemies { get => _containerEnemies; }
    public IActionNPC CurrnetAttacker { get => _currentAttacker; }

    private IActionNPC _currentAttacker;
    private readonly ContainerEnemies _containerEnemies;
    private readonly Player _player;
    private readonly List<IActionNPC> _attackers;

    public QueueAttacker(Player player, ContainerEnemies containerEnemies)
    {
        _player = player;
        _containerEnemies = containerEnemies;
        _currentAttacker = _player;

        _attackers = new List<IActionNPC>(_containerEnemies.Enemies.Count);
        _attackers.AddRange(_containerEnemies.Enemies);
        _attackers.Reverse();
    }

    public void DoAttack(out Socket[] sockets) 
    {
        _currentAttacker = null;

        foreach (var attacker in _attackers)
        {
            if (attacker.IsReady() == true)
            {
                _currentAttacker = attacker;
            }
        }

        if(_currentAttacker == null) 
        {
            _currentAttacker = _player;
        }

        _currentAttacker.MakeMove(out sockets);
    }

    public void DecrementCounters() 
    {
        if(_currentAttacker.Equals(_player))
        { 
            foreach (var attacker in _attackers)
            {
                attacker.DecrementCounter();        
            }
        }
    }
}
