﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BootLoader : Singleton<BootLoader>
{
    public BootLoaderSettings settingsLoader = default;
    public WindowSettings ws = default;

    [SerializeField] private Canvas _mainCanvas;
    public SettingsNodeCollection Settings
    { 
        get
        {
            if(settingsLoader.isExists == false)
                settingsLoader.WriteDefaulteSettings();
            
            return settingsLoader.LoadSettings();
        } 
    }
    
    private void Start() 
    {
        _mainCanvas.GetComponent<CanvasGroup>().interactable = true;
        ApplySettings(Settings);
    }

    private void ApplySettings(SettingsNodeCollection settings)
    {
        if (CurrentSettings.instance == null)
            return;

        CurrentSettings.instance.Platform = Application.platform.ToString();
        

        for (int i = 0; i < settings.settings.Length; i++)
        {
            if (settings.settings[i].nameParam.Equals("BackgroundMusic"))
            {
                CurrentSettings.instance.VolumeBackgroundMuic = float.Parse(settings.settings[i].value);
            }

            if (settings.settings[i].nameParam.Equals("SFX"))
            {
                CurrentSettings.instance.VolumeSFX = float.Parse(settings.settings[i].value);
            }
            if (settings.settings[i].nameParam.Equals("Control"))
            {
                if (settings.settings[i].value.Equals("DesktopPlatform") || settings.settings[i].value.Equals("WindowsEditor"))
                    CurrentSettings.instance.Control = CurrentSettings.Controls.Mouse;
                else if (settings.settings[i].value.Equals("MobilePlatform"))
                    CurrentSettings.instance.Control = CurrentSettings.Controls.Swipe;
            }
        }

        if(SoundSystem.instance != null)
        {
            SoundSystem.instance.GetBackgroundAudioSource().volume = CurrentSettings.instance.VolumeBackgroundMuic;
            SoundSystem.instance.GetSFXAudioSource().volume = CurrentSettings.instance.VolumeSFX;
        }
    }

    public void RunGame()
    {
        _mainCanvas.GetComponent<CanvasGroup>().interactable = false;
        DontDestroyOnLoad(this.gameObject);
        Camera.main.enabled = false;
        SceneManager.LoadScene(2, LoadSceneMode.Additive);
     
    }
}
