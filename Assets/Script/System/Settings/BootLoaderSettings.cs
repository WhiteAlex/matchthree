﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BootLoaderSettings: Singleton<BootLoaderSettings>
{
   private SettingsNodeCollection _settings;
   private string fileName;
   private string settingsPath;
   public bool isExists
      {
         get
         {
            if(File.Exists(settingsPath) == true)
            return true;
            
            return false; 
         }
      }

   private void Start() 
   {

   }
   private void Awake() 
   {
      fileName = "/Settings.json";
      settingsPath = Application.persistentDataPath + fileName;
   }

   public SettingsNodeCollection LoadSettings()
   {
      using(StreamReader stream = new StreamReader(settingsPath))
      {
         string json = stream.ReadToEnd();
         _settings = JsonUtility.FromJson<SettingsNodeCollection>(json);
      }

      return _settings;
   }

   private SettingsNodeCollection GenerateDefaulteSettings()
   {
      SettingsNode[] settings = new SettingsNode[3];
      
      string control = "";

      if(Application.isMobilePlatform)
         control = "MobilePlatform";
      else 
         control = "DesktopPlatform";

      settings[0] = new SettingsNode(){typeParam = "string", nameParam = "Control", value = control};
      settings[1] = new SettingsNode(){typeParam = "float", nameParam = "BackgroundMusic", value = "0,25"};
      settings[2] = new SettingsNode(){typeParam = "float", nameParam = "SFX", value = "0,25"};
      
      return new SettingsNodeCollection(){settings = settings};
   }

   public void WriteSettings(string control, float valueBackgroundMusic, float valueSFX )
   {
      SettingsNode[] settings = new SettingsNode[3];

      settings[0] = new SettingsNode(){typeParam = "string", nameParam = "Control", value = control};
      settings[1] = new SettingsNode(){typeParam = "float", nameParam = "BackgroundMusic", value = valueBackgroundMusic.ToString()};
      settings[2] = new SettingsNode(){typeParam = "float", nameParam = "SFX", value = valueSFX.ToString()};

      _settings = new SettingsNodeCollection(){settings = settings};

      Write(_settings);
   }

   public void WriteDefaulteSettings()
   {
      Write(GenerateDefaulteSettings());
   }



   private void Write(SettingsNodeCollection collection)
   {
      using(StreamWriter stream = new StreamWriter(settingsPath))
      {
         string json = JsonUtility.ToJson(collection);

         stream.Write(json);
      }
   }

}
