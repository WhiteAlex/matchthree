﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class SettingsNode
{
    public string typeParam;
    public string nameParam;
    public string value;
}
