﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SettingsNodeCollection 
{
    public SettingsNode[] settings;

    public int Count{ get{ return settings.Length; } }
    
    public override string ToString()
    {
        string value = "";

        for(int i = 0; i < settings.Length; i++)
        {
            value += "{ "+settings[i].typeParam +" "+ settings[i].nameParam +" "+ settings[i].value+ " }";
        }

        return value;
    }
}
