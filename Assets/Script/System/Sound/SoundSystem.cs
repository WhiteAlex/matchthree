﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSystem : Singleton<SoundSystem>
{
    [SerializeField] private AudioSource _backgroundMusic;
    [SerializeField] private AudioSource _soundFX;

    public void BackgroundMusicValueChanged(Slider newValue)
    {
        _backgroundMusic.volume = newValue.value;
        if (CurrentSettings.instance != null)
            CurrentSettings.instance.VolumeBackgroundMuic = _backgroundMusic.volume;
    }

    public void SoundFXValueChanged(Slider newValue)
    {
        _soundFX.volume = newValue.value;
        if (CurrentSettings.instance != null)
            CurrentSettings.instance.VolumeSFX = _soundFX.volume;
    }

    public AudioSource GetBackgroundAudioSource()
    {
        return _backgroundMusic;
    }

    public AudioSource GetSFXAudioSource() 
    {
        return _soundFX;
    }

}
